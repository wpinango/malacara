import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InputField extends StatelessWidget {
  IconData icon;
  String hintText;
  String prefixText;
  String suffixText;
  TextInputType textInputType;
  Color textFieldColor, iconColor;
  bool obscureText;
  double bottomMargin;
  TextStyle textStyle, hintStyle;
  var validateFunction;
  var onSaved;
  Key key;
  int maxLines;
  FocusNode focusNode;
  TextCapitalization textCapitalization;
  TextEditingController controller;

  //passing props in the Constructor.
  InputField(
      {this.key,
        this.hintText,
        this.obscureText,
        this.textInputType,
        this.textFieldColor,
        this.icon,
        this.iconColor,
        this.bottomMargin,
        this.textStyle,
        this.validateFunction,
        this.onSaved,
        this.hintStyle,
        this.prefixText,
        this.focusNode,
        this.textCapitalization,
        this.controller,
        this.maxLines,
      });

  @override
  Widget build(BuildContext context) {
    return (
        new Container(
        margin: new EdgeInsets.only(bottom: bottomMargin),
        child: new DecoratedBox(
          decoration: new BoxDecoration(
              borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
              color: textFieldColor),
          child: new TextFormField(
            style: textStyle,
            key: key,
            obscureText: obscureText,
            keyboardType: textInputType,
            validator: validateFunction,
            onSaved: onSaved,
            maxLines: maxLines != null? maxLines : 1,
            focusNode: focusNode,
            controller: controller,
            textCapitalization: textCapitalization,
            decoration: new InputDecoration(
              border: InputBorder.none,
              hintText: hintText,
              hintStyle: hintStyle,
              prefixText: prefixText,
              icon: new Icon(
                icon,
                color: iconColor,
              ),
            ),
          ),
        )));
  }
}