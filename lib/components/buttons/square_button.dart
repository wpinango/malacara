import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../theme.dart';

class SquareButton extends StatelessWidget {
  final String buttonName;
  final VoidCallback onTap;
  final Image image;
  final double height;
  final double width;
  final double bottomMargin;
  final double borderWidth;
  final Color buttonColor;

  SquareButton(
      {this.buttonName,
      this.onTap,
      this.height,
      this.bottomMargin,
      this.borderWidth,
      this.width,
      this.image,
      this.buttonColor});

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: height,
      width: width,
      decoration: new BoxDecoration(
        boxShadow: [],
      ),
      child: RawMaterialButton(
          padding: EdgeInsets.all(12.0),
          fillColor: buttonColor,
          splashColor: Colors.grey,
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.grey)),
          textStyle: TextStyle(color: Colors.white),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                child: image,
                height: height / 3,
                width: width / 3,
              ),
              Padding(
                padding: EdgeInsets.all(2),
              ),
              Text(buttonName,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.varelaRound(textStyle:  TextStyle(
                    color: Color(0xFF8a8b8b),
                    fontSize: 9,))),
            ],
          ),
          onPressed: onTap),
    );
  }
}
