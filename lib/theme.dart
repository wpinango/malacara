import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color primaryColor = const Color(0xffffffff);
Color white = const Color(0xffffffff);
Color nexyColor  = const Color(0xff01328e);
Color green = const Color(0xffbbd033);
Color nexiBlue = Color(0xff2b94d1);
Color secondaryColor = Color(0xffdfc187);
Color terciaryColor = Color(0xff000000);
TextStyle textStyle = const TextStyle(
    color: const Color(0xff000000),
    fontSize: 16.0,
    fontWeight: FontWeight.normal);

TextStyle hintStyle = const TextStyle(
    color: Colors.black,
    fontSize: 16.0,
    fontWeight: FontWeight.normal);

TextStyle textDrawerStyle = GoogleFonts.varelaRound(color:  Colors.white);/*TextStyle(
  color: Color(0xffdfc187)
);*/

TextStyle codeStyle = new TextStyle(
  color: const Color(0xfff55020),
  fontSize: 22.0,
  fontWeight: FontWeight.bold,
);

Color whatsappBar = const Color(0xFF41CA50);

Color textFieldColor =  const Color(0xFFEFF0F1);

TextStyle buttonTextStyle = const TextStyle(
    color: Colors.white,
    fontSize: 14.0,
    fontFamily: "Roboto",
    fontWeight: FontWeight.bold);