import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:native_widgets/native_widgets.dart';

class MyDialog{

  static void showNativeDialog<T>({BuildContext context, Widget child}) async {
    await showDialog<T>(
      context: context,
      builder: (BuildContext context) => child,
    );
  }

// Show the Pop Up
  static Future<bool> showNativePopUpWith2Buttons(BuildContext context, String title, String content,
      String negativeText, String positiveText) async {
    bool isPressed = await showDialog<bool>(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => NativeDialog(
            title: Text(title),
            content: Text(content),
            actions: <NativeDialogAction>[
              NativeDialogAction(
                text: Text(negativeText),
                isDestructive: false, // Set True to indicate with red accent
                onPressed: ()  => Navigator.pop(context,false),//Navigator.of(context).pop(false),
              ),
              NativeDialogAction(
                text: Text(positiveText),
                isDestructive: false,  // Set True to indicate with red accent
                onPressed: () => Navigator.pop(context,true),
              ),
            ]
        )
    );
    return isPressed;
  }

  static Future<bool> showSignOutDialog(BuildContext context) async {
    bool isPressed =  await showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alerta'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Desea cerrar sesion'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.pop(context,false);
              },
            ),
            FlatButton(
              child: Text('Si'),
              onPressed: () {
                Navigator.pop(context,true);
                //Routes.replacementScreen(context, '/Login');
              },
            )
          ],
        );
      },
    );
    return isPressed;
  }
}