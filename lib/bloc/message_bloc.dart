import 'dart:async';

import 'package:malacara/bloc/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';

class MessageBloc implements BlocBase {
  /// Sinks
  Sink<Message> get addition => messageAdditionController.sink;
  final messageAdditionController = StreamController<Message>();

  /// Streams
  Stream<Message> get messageStream => _message.stream;
  final _message = BehaviorSubject<Message>();

  MessageBloc() {
    messageAdditionController.stream.listen(handleMessageAdd);
  }

  ///
  /// Logic for message added .
  ///
  void handleMessageAdd(Message msg) {
    _message.add(msg);
    return;
  }

  @override
  void dispose() {
    messageAdditionController.close();
  }
}

class Message {
  final String text;
  final String type;

  Message(this.text, this.type);
}