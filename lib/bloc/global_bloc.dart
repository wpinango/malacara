import 'package:malacara/bloc/shopping_car_bloc.dart';
import 'package:malacara/bloc/bloc_provider.dart';

import 'message_bloc.dart';

class GlobalBloc implements BlocBase {
  ShoppingCartBloc shoppingCartBloc;
  MessageBloc messageBloc;

  GlobalBloc() {
    shoppingCartBloc = ShoppingCartBloc();
    messageBloc = MessageBloc();
  }

  void dispose() {
    shoppingCartBloc.dispose();
    messageBloc.dispose();
  }
}