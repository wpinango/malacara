import 'package:malacara/database/db_configuration.dart';
import 'package:malacara/models/user_order.dart';
import 'package:sembast/sembast.dart';

class OrderDao {
  static const String folderName = "order";
  final orderFolder = intMapStoreFactory.store(folderName);

  Future<Database> get _db async => await AppDatabase.instance.database;

  Future insertOrder(UserOrder userOrder) async {
    await orderFolder.add(await _db, userOrder.parseToOwn());
    print('UserOrder Inserted successfully !!');
  }

  Future updateOrder(UserOrder userOrder) async {
    final finder = Finder(filter: Filter.byKey(userOrder.reference));
    await orderFolder.update(await _db, userOrder.parseToOwn(), finder: finder);
  }

  Future delete(UserOrder userOrder) async {
    final finder = Finder(filter: Filter.byKey(userOrder.reference));
    await orderFolder.delete(await _db);

  }

  Future deleteAll() async {
    List<UserOrder> userOrder = await getAllOrders();
    for (var u in userOrder) {
      delete(u);
    }
  }

  Future<List<UserOrder>> getAllOrders() async {
    final recordSnapshot = await orderFolder.find(await _db);
    return recordSnapshot.map((snapshot) {
      final student = UserOrder.fromJson(snapshot.value);
      return student;
    }).toList();
  }
}
