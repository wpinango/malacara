import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class InformationScreen extends StatefulWidget {
  final status;

  const InformationScreen({Key key, this.status}) : super(key: key);

  @override
  InformationScreenState createState() => new InformationScreenState(status);
}

class InformationScreenState extends State<InformationScreen> {
  InAppWebViewController webView;
  var status;
  String url;
  double progress = 0;

  InformationScreenState(this.status) {
    url = status == 0
        ? 'https://delivery.lamalacara.com/public/policy'
        : 'https://delivery.lamalacara.com/public/policy';
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text( status == 0
            ? 'Terminos y Condiciones'
            : 'Politica de Privacidad'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              if (webView != null) {
                webView.reload();
              }
            },
          )
        ],
      ),
      body: Container(
          child: Column(children: <Widget>[
        Container(
            child: progress < 1.0
                ? LinearProgressIndicator(value: progress)
                : Container()),
        Expanded(
          child: Container(
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: InAppWebView(
              initialUrl: url,
              initialHeaders: {},
              initialOptions: InAppWebViewGroupOptions(
                  crossPlatform: InAppWebViewOptions(
                debuggingEnabled: true,
              )),
              onLoadResource: (controller, resource) {
                print(controller);
                print(resource);
              },
              onWebViewCreated: (InAppWebViewController controller) {
                webView = controller;
              },
              onProgressChanged:
                  (InAppWebViewController controller, int progress) {
                setState(() {
                  this.progress = progress / 100;
                });
              },
            ),
          ),
        ),
      ])),
    );
  }
}
