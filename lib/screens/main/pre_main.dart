import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/theme.dart';

import '../../routes.dart';

class PreMainScreen extends StatefulWidget {
  const PreMainScreen({Key key}) : super(key: key);

  @override
  PreMainScreenState createState() => new PreMainScreenState();
}

const kGoogleApiKey = 'AIzaSyDZZSgj5F6EDyoSwReFKk9DJ3K9a6S4IeM';

class PreMainScreenState extends State<PreMainScreen> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: screenSize.width,
        height: screenSize.height,
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage("assets/row-bgimage-1.jpg"), fit: BoxFit.cover),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Padding(padding: EdgeInsets.all(24),),
            Container(
              child: Image.asset('assets/logo.png'),
              width: screenSize.width / 1.5,
            ),
            new Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
              new RoundedButton(
                buttonName: "Entrar",
                onTap: handleFormSubmitted,
                width: screenSize.width / 1.5,
                height: screenSize.height / 12,
                bottomMargin: 10.0,
                borderWidth: 0.0,
                buttonColor: secondaryColor,
              ),
              Padding(padding: EdgeInsets.all(20),),
             /* Container(
                child: Image.asset('assets/NEXITAXI.png'),
                width: screenSize.width / 3,
                height: screenSize.height / 8,
              ),*/
            ],),
          ],
        ),
      ),
    );
  }

  handleFormSubmitted() {
    Routes.replacementScreen(context, '/FindRest');
  }
}
