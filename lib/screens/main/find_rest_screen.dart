import 'dart:convert';
import 'dart:ui';

import 'package:flutter/rendering.dart';
import 'package:geocoder/geocoder.dart' as geo;
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/address.dart';
import 'package:malacara/models/business.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/credentials.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/models/user_location.dart';
import 'package:malacara/screens/business/business_screen.dart';
import 'package:malacara/screens/country/country_selected_screen.dart';
import 'package:malacara/screens/login/login_screen.dart';
import 'package:malacara/screens/login/logout_screen.dart';
import 'package:malacara/screens/main/information_screen.dart';
import 'package:malacara/screens/profile/profile_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:location/location.dart' as locationData;

import '../../api_url.dart';
import '../../global.dart';
import '../../routes.dart';
import '../../theme.dart';
import '../../util.dart';

class FindRestScreen extends StatefulWidget {
  const FindRestScreen({Key key}) : super(key: key);

  @override
  FindRestState createState() => new FindRestState();
}

const kGoogleApiKey = 'AIzaSyDZZSgj5F6EDyoSwReFKk9DJ3K9a6S4IeM';

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class FindRestState extends State<FindRestScreen> {
  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  List<String> locationNameList = new List();
  String currentText = "";
  TextEditingController locationController = TextEditingController();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  bool isRestaurant = false;
  bool isBusiness = false;
  String country;
  Country selectedCountry = new Country();
  List<Country> countryList = new List();
  var timeout = const Duration(seconds: 20);
  List<String> values = ['España'];
  var location = new locationData.Location();
  bool isLogin = false;
  User user;
  Address address;
  List<Address> addresses = new List();

  @override
  initState() {
    super.initState();
    initVars();
  }

  initVars() async {
    checkUserLogin(-1);
    getCountrySelected();
    getAddresses();
  }

  void getAddresses() async {
    final prefs = await SharedPreferences.getInstance();
    user = User.fromJson(json.decode(prefs.get('user')));
    addresses.clear();
    for (var a in user.address) {
      Address address = new Address();
      address.address = a['address'];
      address.addressId = a['id_direccion'];
      address.cityId = a['id_city'];
      address.lat = a['lat'];
      address.lng = a['lng'];
      address.info = a['info'];
      addresses.add(address);
    }
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        selectedCountry =
            Country.fromJson(json.decode(prefs.get('selectedCountry')));
      });
    }
    setState(() {});
  }

  void getCountrySelected() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        selectedCountry =
            Country.fromJson(json.decode(prefs.get('selectedCountry')));
      });
    }
    /*if (selectedCountry.name == null) {
      requestVersion();
    }*/
  }

  void searchPlace(Prediction p) async {
    PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
    final lat = detail.result.geometry.location.lat;
    final lng = detail.result.geometry.location.lng;

    for (var i = 0; i < p.placeId.length; i++) {
      locationNameList.add(("${p.description} - $lat/$lng"));
    }
  }

  void checkUserLogin(int state) async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('login') != null) {
      isLogin = prefs.getBool('login');
      if (isLogin) {
        user = User.fromJson(json.decode(prefs.get('user')));
        setState(() {

        });
        if (state != 1) {
          refreshData();
        }
      }
    }
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void requestFind(double lat, double lng) async {
    showProgressDialog();
    try {
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      String url = ApiURL.apiURL; //prefs.get(Global.keyApiUrl);
      String token;
      if (isLogin) {
        User user = User.fromJson(json.decode(prefs.get('user')));
        print(json.encode(user));
        token = user.token;
      } else {
        token = '-1';
      }
      int type;
      if (isBusiness) {
        type = 2;
      } else if (isRestaurant) {
        type = 1;
      }
      var body = {'lat': lat, 'lng': lng, "type": 1, 'token': token};
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post(url + ApiURL.urlGetBusinessList.replaceAll('?', apiVersion ?? "v050100"),
              headers: headers, body: map)
          .timeout(timeout);
      if (response.body != "") {
        handleFindResponse(response.body);
      } else {
        handleFindResponse("");
      }
    } catch (e) {
      handleFindResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleFindResponse(final responseBody) async {
    cancelProgressDialog();
    print('valores ' + responseBody);
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      if (map['resultado']['code'] == 0) {
        if (map['resultado']['total_negocios'] != 0) {
          try {
            print(responseBody);
            var responseJson = json.decode(responseBody);
            List<Business> business =
                (responseJson['resultado']['negocios'] as List)
                    .map((p) => Business.fromJson(p))
                    .toList();
            if (await Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => new BusinessScreen(
                          business: business,
                        )))) {
              getAddresses();
              setState(() {});
              checkUserLogin(1);
            }
            address = null;
          } catch (e) {
            e.toString();
          }
        } else {
          /*await Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new BusinessScreen(
                        business: null,
                      )));
          getAddresses();*/

          if (await Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => new BusinessScreen(
                        business: null,
                      )))) {
            getAddresses();
            setState(() {});
          }
        }
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('No hay negocios en esta zona');
      } else if (map['resultado']['code'] == -3) {
        /*if (await MyDialog.showNativePopUpWith2Buttons(context, 'Advertencia', 'Existe un probema con su sesion, debe iniciar sesion nuevamente', '', 'Salir')) {

        }*/
        refreshData();
      }
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  void refreshData() async {
    showProgressDialog();
    final prefs = await SharedPreferences.getInstance();
    Credentials credentials =
        Credentials.fromJson(json.decode(prefs.get(Global.keyCredentials)));
    print(credentials.user);
    var body = {
      'username': credentials.user.trim(),
      'password': Util.generateMd5(credentials.password),
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    String apiVersion = prefs.get('apiVersion');
    try {
      final response = await http
          .post(ApiURL.apiURL + ApiURL.doLogin.replaceAll('?', apiVersion),
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleRefreshDataResponse(response.body);
      } else {
        handleRefreshDataResponse("");
      }
    } catch (e) {
      handleRefreshDataResponse("");
    }
  }

  void handleRefreshDataResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        print(json.encode(map['resultado']));
        user = User.fromJson(json.decode(json.encode(map['resultado'])));
        final prefs = await SharedPreferences.getInstance();
        print(json.encode(user));
        setState(() {
          prefs.setString('user', json.encode(user));
          prefs.setBool('login', true);
        });
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('El usuario o contraseña son incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio temporalmente no disponible');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    var height = AppBar().preferredSize.height;
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Container(
            height: height / 1.8,
            child: Image(
              image: AssetImage('assets/logo.png'),
            )),
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      drawer: getDrawer(screenSize),
      key: scaffoldKey,
      body: ModalProgressHUD(
        child: SingleChildScrollView(
            controller: scrollController,
            child: new Container(
              height: screenSize.height,
              //padding: new EdgeInsets.only(left: 8, right: 8, top: 8),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/bg-coffee-shop.jpg"),
                    fit: BoxFit.cover),
              ),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
                child: Container(
                  padding: new EdgeInsets.only(left: 8, right: 8, top: 8),
                  width: screenSize.width,
                  color: Colors.black.withOpacity(0.5),
                  child: new Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 8, right: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                CircleAvatar(
                                  backgroundColor: secondaryColor,
                                  radius: 25,
                                  child: user != null
                                      ? Text(
                                          user.name.substring(0, 1) +
                                              user.lastName.substring(0, 1),
                                          style: TextStyle(color: white),
                                        )
                                      : Text(
                                          'NV',
                                          style: TextStyle(color: white),
                                        ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 16),
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        'Hola ',
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(
                                                fontSize: 20, color: white)),
                                      ),
                                      Text(
                                        user != null ? user.name : '',
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(
                                                fontSize: 20, color: secondaryColor)),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      Container(
                        padding: EdgeInsets.all(12),
                        width: screenSize.width / 1.1,
                        height: screenSize.height / 14,
                        decoration: BoxDecoration(
                          color: secondaryColor,
                          borderRadius:
                              new BorderRadius.all(const Radius.circular(5.0)),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'RESTAURANTE-',
                              style: GoogleFonts.varelaRound(
                                  textStyle: TextStyle(
                                      color: white,
                                      fontSize: screenSize.width / 25,
                                      fontWeight: FontWeight.bold)),
                            ),
                            Text(
                              'DELIVERY-COFFEE SHOP',
                              style: GoogleFonts.varelaRound(
                                  textStyle: TextStyle(
                                      color: white,
                                      fontSize: screenSize.width / 25,
                                      fontWeight: FontWeight.bold)),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      addresses.isNotEmpty
                          ? Container(
                              margin: new EdgeInsets.only(right: 8, left: 8),
                              //width: screenSize.width / 1.1,
                              height: screenSize.height / 12,
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                    style: BorderStyle.solid, width: 0.80),
                              ),
                              child: new DropdownButtonHideUnderline(
                                child: DropdownButton<Address>(
                                  isExpanded: true,
                                  hint: Text(
                                    'Selecciona una direccion', style: GoogleFonts.varelaRound(),
                                    maxLines: 1,
                                    softWrap: false,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  items: addresses
                                      .map<DropdownMenuItem<Address>>(
                                          (Address value) {
                                    return DropdownMenuItem<Address>(
                                      value: value,
                                      child: Text(
                                        value.address,
                                        maxLines: 1,
                                        softWrap: false,
                                        style: GoogleFonts.varelaRound(),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (Address value) {
                                    setState(() {
                                      address = value;
                                      prepareDataLocation(
                                          double.parse(address.lat),
                                          double.parse(address.lng));
                                      //requestFind(double.parse(address.lat), double.parse(address.lng));
                                    });
                                  },
                                  value: address,
                                  isDense: true,
                                ),
                              ))
                          : null,
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      GestureDetector(
                        child: Container(
                          margin: new EdgeInsets.only(right: 8, left: 8),
                          padding: new EdgeInsets.only(right: 8, left: 8),
                          decoration: BoxDecoration(
                            color: white,//Color(0xFFf4f4f4),
                            borderRadius:
                            new BorderRadius.all(const Radius.circular(5.0)),
                          ),
                          height: screenSize.height / 12,
                          //color: Color(0xFFf4f4f4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Ver Restaurante La Malacara',
                                style: GoogleFonts.varelaRound(),
                              ),
                              Icon(Icons.arrow_forward_ios_outlined)
                            ],
                          ),
                        ),
                        onTap: () {
                          onSearchButtonPressed();
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                        ].where((child) => child != null).toList(),
                      ),
                    ].where((child) => child != null).toList(),
                  ),
                ),
              ),
            )),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  Widget getBottomBar(Size s) {
    if (selectedCountry.name != null) {
      return GestureDetector(
        child: SizedBox(
          height: s.height / 12,
          width: s.width,
          child: BottomAppBar(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new IconButton(
                  icon: new Image.asset('assets/ic_whatsapp.png'),
                  tooltip: 'Closes application',
                  onPressed: () => showWhatsAppDialog(),
                  iconSize: 18.0,
                ),
                Text(
                  '¿Prefieres pedir por WahtsApp?',
                  style: TextStyle(color: white, fontSize: 16),
                ),
              ],
            ),
            color: whatsappBar,
          ),
        ),
        onTap: () {
          showWhatsAppDialog();
        },
      );
    }
  }

  Widget getDrawer(Size screenSize) {
    Widget drawer;
    if (selectedCountry.name != null) {
      if (isLogin) {
        drawer = new Theme(
            data: Theme.of(context).copyWith(
              canvasColor: primaryColor,
            ),
            child: new Drawer(
                child: Container(
                  decoration: BoxDecoration(
                      image:
                      DecorationImage(image: AssetImage("assets/single-img-two.jpg"), fit: BoxFit.cover )
                    //color: primaryColor,
                  ),
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
                    child: Container(
                      color: Colors.black.withOpacity(0.8),
                      child: new ListView(
              children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      new Center(
                          child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Container(
                              width: 130,
                              height: 130,
                              decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  image: new DecorationImage(
                                      fit: BoxFit.fill,
                                      image: new ExactAssetImage(
                                          'assets/footer-logo.png')))),
                          Padding(padding: EdgeInsets.all(8)),
                          new Text(user.name + " " + user.lastName,
                              style: TextStyle(color: secondaryColor), textScaleFactor: 1.5)
                        ],
                      )),
                      Padding(padding: EdgeInsets.all(8)),
                      new ListTile(
                          leading: new Icon(
                            Icons.account_circle,
                            color: white,
                          ),
                          title: new Text(
                            'Mi cuenta',
                            style: textDrawerStyle,
                          ),
                          onTap: () {
                            Navigator.pop(context);
                            openProfileScreen(); //onPressed(context, '/Profile');
                          }),
                      new ListTile(
                          leading: new Icon(Icons.shopping_basket, color: white),
                          title: new Text('Pedidos', style: textDrawerStyle),
                          onTap: () {
                            Navigator.pop(context);
                            Routes.showModalScreen(context, '/OrderHistory');
                          }),
                      new ListTile(
                          leading: new Icon(Icons.work, color: white),
                          title: new Text('Terminos y Condiciones',
                              style: textDrawerStyle),
                          onTap: () {
                            Navigator.pop(context);
                            openWebViewInformation(0);
                          }),
                      new ListTile(
                          leading: new Icon(Icons.lock, color: white),
                          title: new Text('Politica de Privacidad',
                              style: textDrawerStyle),
                          onTap: () {
                            Navigator.pop(context);
                            openWebViewInformation(1);
                          }),
                      new ListTile(
                          leading: new Icon(Icons.exit_to_app, color: white),
                          title: new Text('Cerrar sesion',
                              style: textDrawerStyle),
                          onTap: () {
                            Navigator.pop(context);
                            onLogoutMenuSelect();
                          }),

              ],
            ),
                    ),
                  ),
                )));
      } else {
        drawer = new Drawer(
            child:
            new Container(
                width: screenSize.width / 4,
                height: screenSize.height / 4.5,
                decoration: new BoxDecoration(
                    shape: BoxShape.rectangle,
                    image:
                    DecorationImage(image: AssetImage("assets/single-img-two.jpg"), fit: BoxFit.cover )),
                child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
                    child: Container(
                        color: Colors.black.withOpacity(0.8),
                        child: new ListView(
                          children: <Widget>[
                            new Center(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                        width: 130,
                                        height: 130,
                                        decoration: new BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            image: new DecorationImage(
                                                fit: BoxFit.fill,
                                                image: new ExactAssetImage(
                                                    'assets/footer-logo.png')))),
                                    Padding(padding: EdgeInsets.all(8)),
                                  ],
                                )),
                            new ListTile(
                                leading: new Icon(Icons.input, color: white,),
                                title: new Text('Iniciar sesion', style: textDrawerStyle,),
                                onTap: () {
                                  Navigator.pop(context);
                                  onLoginButtonPressed();
                                }),
                            new ListTile(
                                leading: new Icon(Icons.assignment_ind, color: white,),
                                title: new Text(
                                  'Eres nuevo?, Registrate!',
                                    style: textDrawerStyle
                                ),
                                onTap: () {
                                  Navigator.pop(context);
                                  onPressed(context, '/SignUp');
                                }),
                            /*new ListTile(
                leading: new Icon(Icons.public),
                title: new Text('Seleccionar pais'),
                onTap: () {
                  Navigator.pop(context);
                  openCountryScreen();
                }),*/
                          ],
                        )
                    ))
            ),);
      }
    } else {
      drawer = new Drawer(
        child: Center(
          child: new Container(
              alignment: Alignment.center,
              width: screenSize.width / 2,
              height: screenSize.height / 6,
              decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: ExactAssetImage('assets/logo_naranja.png')))),
        ),
      );
    }

    return SizedBox(
      child: drawer,
      width: screenSize.width / 1.2,
    );
  }

  void openWebViewInformation(var status) {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new InformationScreen(
                  status: status,
                )));
  }

  void openProfileScreen() async {
    if (await Navigator.push(context,
        new MaterialPageRoute(builder: (context) => new ProfileScreen()))) {
      getAddresses();
      setState(() {});
    }
  }

  void openCountryScreen() async {
    if (await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new CountrySelectedScreen()))) {
      getCountrySelected();
      setState(() {});
    }
  }

  Widget getButtons(Size screenSize) {
    Widget w = new Container();
    try {
      if (isRestaurant || isBusiness) {
        w = new Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 16.0)),
              new SizedBox(
                  width: screenSize.width / 1.30,
                  child: OutlineButton.icon(
                      onPressed: getCurrentLocation,
                      icon: new Icon(
                        Icons.my_location,
                        color: white,
                      ),
                      label: Text(
                        'Usar mi ubicacion',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ))),
              Padding(padding: EdgeInsets.only(top: 16.0)),
              new SizedBox(
                  width: screenSize.width / 1.30,
                  child: OutlineButton.icon(
                      onPressed: onSearchButtonPressed,
                      icon: new Icon(
                        Icons.arrow_forward_ios_outlined,
                        color: white,
                      ),
                      label: Text(
                        'Ver Restaurante La Malacara',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ))),
            ],
          ),
        );
      }
    } catch (e) {
      print(e);
    }
    return w;
  }

  onButtonPressed() async {
    if (country != null) {
      for (var c in countryList) {
        if (c.name == country) {
          setState(() {
            selectedCountry = c;
          });
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('selectedCountry', json.encode(selectedCountry));
          prefs.setString(Global.keyApiUrl,
              selectedCountry.protocol + '://' + selectedCountry.url);
        }
      }
    } else {
      showInSnackBar("Debe seleccionar un pais");
    }
  }

  onPressed(BuildContext context, String routeName) {
    Routes.showModalScreen(context, routeName);
  }

  onLoginButtonPressed() async {
    int result = await Navigator.push(context,
        new MaterialPageRoute(builder: (context) => new LoginScreen(isOnlyFinish: true,)));
    if (result == 1) {
      checkUserLogin(result);
    }
  }

  void onLogoutMenuSelect() async {
    //Routes.showModalScreen(context, '/Logout');
    bool result = await Navigator.push(context,
        new MaterialPageRoute(builder: (context) => new LogoutScreen()));
    if (result) {
      Routes.replacementScreen(context, '/Splash');
    }
  }

  void showWhatsAppDialog() async {
    String
        number; //= selectedCountry.code == 'ec' ? '0962640056' : '693220472';
    switch (selectedCountry.code) {
      case 'ec':
        number = '0962640056';
        break;
      case 'es':
        number = '34672309164';
        break;
      case 'pa':
        number = '50760607478';
        break;
    }
    if (await MyDialog.showNativePopUpWith2Buttons(
        context,
        'Ya sabes que pedir?',
        'Escribenos por whatsapp al $number ',
        'Cancelar',
        'Abrir WhatsApp')) {}
  }

  void onSearchButtonPressed() async {
    findCoordinatesFromAddress("Calle virgen del sagrario, Madrid");
    /*Prediction p = await PlacesAutocomplete.show(
        context: context,
        apiKey: kGoogleApiKey,
        mode: Mode.fullscreen,
        logo: Text(''),
        hint: 'Introduce tu direccion',
        language: selectedCountry.completeCode.split('-')[0],
        components: [new Component(Component.country, selectedCountry.code)]);
    PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
    prepareDataLocation(detail.result.geometry.location.lat,
        detail.result.geometry.location.lng);*/
  }

  void prepareDataLocation(double lat, double lng) async {
    UserLocation userLocation = new UserLocation();
    userLocation.latitude = lat;
    userLocation.longitude = lng;
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('location', json.encode(userLocation));
    requestFind(userLocation.latitude, userLocation.longitude);
  }

  void getCurrentLocation() async {
    Map<String, double> l = await getLocation();
    requestFind(l['latitude'], l['longitude']);
  }

  Future<Map<String, double>> getLocation() async {
    var currentLocation;
    try {
      currentLocation = await location.getLocation();
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }

  void findCoordinatesFromAddress(String address) async {
    var coordinates = await geo.Geocoder.local.findAddressesFromQuery(address);
    prepareDataLocation(coordinates.first.coordinates.latitude, coordinates.first.coordinates.longitude);
  }
}
