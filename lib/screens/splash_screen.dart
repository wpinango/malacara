import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/screens/main/find_rest_screen.dart';
import 'package:malacara/screens/payment/webview_screen.dart';
import 'package:malacara/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

import 'login/login_screen.dart';
import 'main/pre_main.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key key}) : super(key: key);

  @override
  FirstScreenState createState() => new FirstScreenState();
}

const kGoogleApiKey = 'AIzaSyDZZSgj5F6EDyoSwReFKk9DJ3K9a6S4IeM';

class FirstScreenState extends State<FirstScreen> {
  bool isLogin = false;

  @override
  void initState() {
    checkUserLogin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new SplashScreen(
      seconds: 4,
      navigateAfterSeconds: isLogin ? new FindRestScreen() : new PreMainScreen(),
      image: new Image.asset('assets/footer-logo.png'),
      imageBackground: AssetImage("assets/col-bgimage-2.jpg"),
      loadingText: Text('Cargando...', style: GoogleFonts.varelaRound(textStyle: TextStyle(color: white)),),
      styleTextUnderTheLoader: new TextStyle(color: white),
      photoSize: 100.0,
      onClick: () => print("Lotosuite"),
      loaderColor: secondaryColor,
    );
  }

  void checkUserLogin() async {
    final prefs = await SharedPreferences.getInstance();
    Country selectedCountry = new Country();
    selectedCountry.name = 'España';
    selectedCountry.url = "lamalacara.com/delivery/panel/ws/aloha24";
    selectedCountry.protocol = "https";
    selectedCountry.code = "es";
    selectedCountry.completeCode = "es-es";
    prefs.setString('selectedCountry', json.encode(selectedCountry));
    if (prefs.getBool('login') != null) {
      setState(() {
        isLogin = prefs.getBool('login');
      });
    }
  }
  
}