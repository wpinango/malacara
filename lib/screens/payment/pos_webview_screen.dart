import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:malacara/api_url.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/user_order.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:io' show Platform;

import '../../global.dart';

class PosWebViewScreen extends StatefulWidget {
  final UserOrder userOrder;

  const PosWebViewScreen({Key key, this.userOrder}) : super(key: key);

  @override
  PosWebViewScreenState createState() =>
      new PosWebViewScreenState(userOrder: userOrder);
}

class PosWebViewScreenState extends State<PosWebViewScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  InAppWebViewController webView;
  String url;
  double progress = 0;
  bool isInAsyncCall = false;
  UserOrder userOrder;
  bool isLoad = false;
  String signature;
  String merchantParameters;
  String html;
  bool isDone = false;

  PosWebViewScreenState({this.userOrder}) {
    url = ApiURL.apiUrlPosPayment;
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  @override
  void initState() {
    super.initState();
    requestEncodeData();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop1,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Pago con tarjeta'),
          actions: <Widget>[
            isDone
                ? IconButton(
                    icon: Icon(Icons.done),
                    onPressed: () {
                      Navigator.pop(context, true);
                    },
                  )
                : IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: () {
                      requestEncodeData();
                      /*if (webView != null) {
                  webView.reload();
                }*/
                    },
                  )
          ],
        ),
        body: ModalProgressHUD(
          child: Container(
              child: isLoad
                  ? Stack(children: <Widget>[
                      Container(
                          child: progress < 1.0
                              ? LinearProgressIndicator(value: progress)
                              : Container()),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blueAccent)),
                          child: InAppWebView(
                            initialUrl: new Uri.dataFromString(html,
                                    mimeType: 'text/html')
                                .toString(),
                            initialHeaders: {},
                            initialOptions: InAppWebViewGroupOptions(
                                crossPlatform: InAppWebViewOptions(
                              javaScriptEnabled: true,
                              debuggingEnabled: true,
                            )),
                            onLoadResource: (controller, resource) {
                              print(controller);
                              print(resource);
                            },
                            onWebViewCreated:
                                (InAppWebViewController controller) {
                              webView = controller;
                            },
                            onLoadStart: (InAppWebViewController controller,
                                String url) {
                              setState(() {
                                print(controller);
                                print(url);
                                if (url.contains('ok')) {
                                  isDone = true;
                                  showInSnackBar(
                                      'Pago procesado, presione salir para terminar la orden');
                                }
                                if (url.contains('ko')) {
                                  showInSnackBar(
                                      'El pago no pudo ser procesado');
                                }
                              });
                            },
                            onProgressChanged:
                                (InAppWebViewController controller,
                                    int progress) {
                              setState(() {
                                this.progress = progress / 100;
                              });
                            },
                          ),
                        ),
                      ),
                      Platform.isAndroid
                          ? Positioned(
                              bottom: 86,
                              right: 16,
                              child: FloatingActionButton(
                                child: Icon(Icons.zoom_in),
                                onPressed: () {
                                  webView.android.zoomIn();
                                },
                              ),
                            )
                          : Container(),
                      Platform.isAndroid
                          ? Positioned(
                              bottom: 24,
                              right: 16,
                              child: FloatingActionButton(
                                child: Icon(Icons.zoom_out),
                                onPressed: () {
                                  webView.android.zoomOut();
                                },
                              ),
                            )
                          : Container(),
                    ])
                  : Container()),
          inAsyncCall: isInAsyncCall,
          opacity: 0.5,
          progressIndicator: NativeLoadingIndicator(),
        ),
        floatingActionButton: Column(
          children: <Widget>[
            /*FloatingActionButton(
              child: Icon(Icons.zoom_in),
              onPressed: () {
              webView.android.zoomIn();
            },),*/
          ],
        ),
      ),
    );
  }

  void requestEncodeData() async {
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    String reference = userOrder.reference.substring(0, 10);
    var data = {'ref': reference, 'total': userOrder.totalAmount.toString()};
    var body = {
      "datos": json.encode(data),
    };

    print(jsonEncode(body));
    Map<String, String> headers = {
      //'Content-Type': 'application/json',
    };
    showProgressDialog();
    try {
      final response = await http
          .post(
              ApiURL.apiURL + ApiURL.urlEncodedData.replaceAll('?', apiVersion),
              headers: headers,
              body: body)
          .timeout(Global.timeout);
      print(response.body);
      cancelProgressDialog();
      if (response.body != null) {
        html = response.body;
        isLoad = true;
        setState(() {});
      } else {
        showInSnackBar('algo salio mal');
      }
    } catch (e) {
      e.toString();
      cancelProgressDialog();
    }
  }

  Future<bool> onWillPop1() async {
    if (isDone) {
      Navigator.pop(context, true);
    } else {
      if (!await MyDialog.showNativePopUpWith2Buttons(context, 'Advertencia',
          '¿Desea cancelar la operacion?', 'Salir', 'Continuar')) {
        Navigator.pop(context, false);
      }
    }
  }

  String _loadHTML() {
    return '''
          <form id="fform" action="https://sis-t.redsys.es:25443/sis/rest/trataPeticionREST" method="post">
            <input type="hidden" name="Ds_SignatureVersion" value="HMAC_SHA256_V1">
            <input type="hidden" name="Ds_MerchantParameters" value="$merchantParameters">
            <input type="hidden" name="Ds_Signature" value="$signature">
          </form>
 
      <script>
        document.getElementById("fform").submit();
      </script>
    ''';
  }
}
