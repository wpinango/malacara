import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/buttons/rounded_button.dart';

import '../../routes.dart';
import '../../theme.dart';

class PaymentSuccessScreen extends StatefulWidget {
  const PaymentSuccessScreen({Key key}) : super(key: key);

  @override
  PaymentSuccessScreenState createState() => new PaymentSuccessScreenState();
}

class PaymentSuccessScreenState extends State<PaymentSuccessScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  bool autoValidate = false;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Pedido realizado',
          style: new TextStyle(
            color: secondaryColor,
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        leading: new Container(),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: new Container(
          height: screenSize.height,
          padding: new EdgeInsets.only(top: 8, right: 8, left: 8, bottom: 8),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: screenSize.width / 2,
                  height: screenSize.height / 12,
                  decoration: new BoxDecoration(
                      shape: BoxShape.rectangle,
                      image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: new ExactAssetImage(
                              'assets/logo.png')))),
              Padding(padding: EdgeInsets.all(16)),
              Text(
                'Muchas gracias por haber realizado tu pedido en La Malacara',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Padding(padding: EdgeInsets.all(8)),
              Text(
                  'Recuerda que los tiempos de entrega no solamente dependen de la disponibilidad de repartidores sino de'
                  'la afluencia que tienen los locales en este momento además de otros factores.'),
              Padding(
                padding: EdgeInsets.all(24),
              ),
              RoundedButton(
                buttonName: 'Nueva busqueda',
                onTap: () {
                  //openPaymentScreen();
                  Routes.replacementScreen(context, "/FindRest");
                },
                width: screenSize.width / 1.1,
                height: screenSize.height / 13,
                bottomMargin: 10.0,
                borderWidth: 1.0,
                buttonColor: secondaryColor,
              )
            ],
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }
}
