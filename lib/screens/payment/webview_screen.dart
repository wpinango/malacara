import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/user_order.dart';
import 'package:malacara/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../global.dart';

class WebViewScreen extends StatefulWidget {
  final UserOrder userOrder;
  final String currencyCode;

  const WebViewScreen({Key key, this.userOrder, this.currencyCode})
      : super(key: key);

  @override
  WebViewScreenState createState() =>
      new WebViewScreenState(userOrder, currencyCode);
}

class WebViewScreenState extends State<WebViewScreen> {
  InAppWebViewController webView;
  UserOrder userOrder;
  String baserUrl = Global.isPaypalProduction
      ? 'https://www.paypal.com/cgi-bin/webscr?business='
      : 'https://www.sandbox.paypal.com/cgi-bin/webscr?business=';
  String url; //TODO PROBAR este cambio antes de lanzar
  double progress = 0;
  bool isDone = false;
  String business = Global.isPaypalProduction
      ? 'administracion@lamalacara.com'
      : 'sb-kyvt31322298@business.example.com';
  String amount;
  String currencyCode;
  String items = '';

  WebViewScreenState(this.userOrder, this.currencyCode) {
    //getCurrency();
    amount = userOrder.totalAmount.toString();
    for (var order in userOrder.order) {
      for (var product in order.products) {
        items += product.name.toString().replaceAll(" ", "+");
      }
    }
    url = baserUrl +
        business +
        '&cmd=_xclick&item_name=' +
        items +
        '&amount=' +
        amount +
        '&currency_code=' +
        currencyCode;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop1,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Paypal payment'),
          actions: <Widget>[
            isDone
                ? IconButton(
              icon: Icon(Icons.done),
              onPressed: () {
                Navigator.pop(context, true);
              },
            )
                : IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                if (webView != null) {
                  webView.reload();
                }
              },
            )
          ],
        ),
        body: Container(
            child: Column(children: <Widget>[
              Container(
                  child: progress < 1.0
                      ? LinearProgressIndicator(value: progress)
                      : Container()),
              Expanded(
                child: Container(
                  decoration:
                  BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                  child: InAppWebView(
                    initialUrl: url,
                    initialHeaders: {},
                    initialOptions: InAppWebViewGroupOptions(
                        crossPlatform: InAppWebViewOptions(
                          debuggingEnabled: true,
                        )),
                    onLoadResource: (controller, resource) {
                      print(controller);
                      print(resource);
                    },
                    onWebViewCreated: (InAppWebViewController controller) {
                      webView = controller;
                    },
                    onLoadStart: (InAppWebViewController controller, String url) {
                      setState(() {
                        print(controller);
                        this.url = url;
                      });
                    },
                    onLoadStop:
                        (InAppWebViewController controller, String url) async {
                      setState(() {
                        this.url = url;
                        print('segunda url : ' + url);
                        print(controller);
                        if (url.contains("done")) {
                          setState(() {
                            isDone = true;
                          });
                        }
                      });
                    },
                    onProgressChanged:
                        (InAppWebViewController controller, int progress) {
                      setState(() {
                        this.progress = progress / 100;
                      });
                    },
                  ),
                ),
              ),
            ])),
      ),
    );
  }

  Future<bool> onWillPop1() async {
    if (isDone) {
      Navigator.pop(context, true);
    } else {
      if (!await MyDialog.showNativePopUpWith2Buttons(context, 'Advertencia',
          '¿Desea cancelar la operacion?', 'Salir', 'Continuar')) {
        Navigator.pop(context, false);
      }
    }
  }
}
