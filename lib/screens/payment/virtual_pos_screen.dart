import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/credentials.dart';
import 'package:malacara/models/restaurant.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/models/user_order.dart';
import 'package:malacara/service/validations.dart';
import 'package:malacara/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:tripledes/tripledes.dart';
import 'dart:convert' show utf8, base64;
import 'package:crypto/crypto.dart';

import '../../api_url.dart';
import '../../global.dart';

class VirtualPosScreen extends StatefulWidget {
  final UserOrder userOrder;
  final String currencyCode;

  const VirtualPosScreen({Key key, this.userOrder, this.currencyCode})
      : super(key: key);

  @override
  VirtualPosScreenState createState() => new VirtualPosScreenState(
      userOrder: userOrder, currencyCode: currencyCode);
}

class VirtualPosScreenState extends State<VirtualPosScreen> {
  UserOrder userOrder;
  String currencyCode;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  Validations validations = new Validations();
  String name; // = "Gabriel Lee";
  String number = '4548812049400004'; //"4111111111111111";
  String expMonth = '12'; // = "02";
  String expYear = '20'; // = "26";
  String cvc = '123'; //= "410";
  String token = "";
  String deviceSessionId = "";
  String merchantId = 'ms2gyt71zjkr7jtkc3g4'; //'mfafzaihg8klc9ab299r';
  String publicKey =
      'pk_7c36290430474f0da7bebf2137524cc0'; //'pk_4968441225774ccaa5a39b8c3869913e';
  String privateKey = 'sk_cf0938b47593409681f3b8aa7ad9685c:';
  var timeout = const Duration(seconds: 60);
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isInAsyncCall = false;
  bool isDone = false;
  bool autoValidate = false;

  VirtualPosScreenState({this.userOrder, this.currencyCode});

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  Future<void> requestDeviceSessionId() async {
    // showProgressDialog();
    // String deviceSessionId;
    //
    // try {
    //   deviceSessionId = await FlutterOpenpay.getDeviceSessionId(
    //     merchantId: merchantId,
    //     publicApiKey: publicKey,
    //     productionMode: false,
    //   );
    //   setState(() {
    //     this.deviceSessionId = deviceSessionId;
    //   });
    //   print(deviceSessionId);
    //   submit();
    // } catch (e) {
    //   print(e.toString());
    //   deviceSessionId = "Unable to tokenize card";
    //   setState(() {
    //     this.deviceSessionId = deviceSessionId;
    //   });
    // }
  }

  Future<void> submit() async {
    // if (formKey.currentState.validate()) {
    //   formKey.currentState.save();
    //
    //   String token;
    //
    //   try {
    //     token = await FlutterOpenpay.tokenizeCard(
    //       cardholderName: name,
    //       cardNumber: number,
    //       cvv: cvc,
    //       expiryMonth: expMonth,
    //       expiryYear: expYear,
    //       publicApiKey: publicKey,
    //       merchantId: merchantId,
    //       productionMode: false,
    //     );
    //   } catch (e) {
    //     print(e.toString());
    //     token = "Unable to tokenize card";
    //   }
    //
    //   setState(() {
    //     this.token = token;
    //   });
    //   print(this.token);
    // }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: onWillPop1,
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: new Text(
            'Pago con tarjeta',
            style: new TextStyle(
              color: Colors.white,
              fontSize: Theme.of(context).platform == TargetPlatform.iOS
                  ? 17.0
                  : 20.0,
            ),
          ),
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back), onPressed: () => onWillPop1()),
        ),
        body: ModalProgressHUD(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(16),
              child: new Form(
                key: formKey,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(height: 16),
                    Container(
                      child: TextFormField(
                        initialValue: name,
                        //autofocus: true,
                        keyboardType: TextInputType.text,
                        enabled: true,
                        decoration: InputDecoration(
                            labelText: "Nombre en la tarjeta",
                            border: OutlineInputBorder()),
                        onSaved: (String value) {
                          name = value;
                        },
                        validator: validations.validateCardNameField,
                      ),
                    ),
                    SizedBox(height: 16),
                    Container(
                      child: TextFormField(
                        initialValue: number,
                        keyboardType: TextInputType.number,
                        maxLength: 16,
                        enabled: true,
                        decoration: InputDecoration(
                            labelText: "Numero de Tarjeta",
                            border: OutlineInputBorder()),
                        onSaved: (String value) {
                          number = value;
                        },
                        validator: validations.validateCardNumberField,
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: TextFormField(
                            initialValue: expMonth,
                            maxLength: 2,
                            keyboardType: TextInputType.number,
                            enabled: true,
                            decoration: InputDecoration(
                                labelText: "MM", border: OutlineInputBorder()),
                            onSaved: (String value) {
                              expMonth = value;
                            },
                            validator: validations.validateExpMonthField,
                          ),
                        ),
                        SizedBox(width: 20),
                        Expanded(
                          flex: 4,
                          child: TextFormField(
                            initialValue: expYear,
                            maxLength: 2,
                            keyboardType: TextInputType.number,
                            enabled: true,
                            decoration: InputDecoration(
                                labelText: "AA", border: OutlineInputBorder()),
                            onSaved: (String value) {
                              expYear = value;
                            },
                            validator: validations.validateExpYearField,
                          ),
                        ),
                        SizedBox(width: 20),
                        Expanded(
                          flex: 3,
                          child: TextFormField(
                            initialValue: cvc,
                            maxLength: 3,
                            keyboardType: TextInputType.number,
                            enabled: true,
                            decoration: InputDecoration(
                                labelText: "CVV", border: OutlineInputBorder()),
                            onSaved: (String value) {
                              cvc = value;
                            },
                            validator: validations.validateBackNumberField,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      width: 240,
                      height: 48,
                      child: new RaisedButton(
                        child: new Text(
                          'Realizar Pago',
                          style: new TextStyle(color: Colors.white),
                        ),
                        onPressed: () => handleFormSubmitted(),
                        color: Colors.blue,
                      ),
                      margin: new EdgeInsets.only(top: 16.0),
                    ),
                    SizedBox(height: 16),
                  ],
                ),
              ),
            ),
          ),
          inAsyncCall: isInAsyncCall,
          opacity: 0.5,
          progressIndicator: NativeLoadingIndicator(),
        ),
      ),
    );
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      requestEncodeData();
      //requestDeviceSessionId();
      //requestPayment();
      //requestDeviceSessionId();
    }
  }

  void requestEncodeData() async {
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    String reference = userOrder.reference.substring(0, 10);
    var data = {'ref': reference, 'total': userOrder.totalAmount.toString()};
    var body = {
      "datos": json.encode(data),
    };

    print(jsonEncode(body));
    Map<String, String> headers = {
      //'Content-Type': 'application/json',
    };
    showProgressDialog();
    try {
      final response = await http
          .post(
              ApiURL.apiURL + ApiURL.urlEncodedData.replaceAll('?', apiVersion),
              headers: headers,
              body: body)
          .timeout(timeout);
      print(response.body);
      cancelProgressDialog();
      if (response.body != null) {
        var decodedResponse = jsonDecode(response.body);
        print(decodedResponse);
        if (decodedResponse != null && decodedResponse != '') {
          requestPayment(decodedResponse);
        }
      } else {
        showInSnackBar('algo salio mal');
      }
    } catch (e) {
      e.toString();
      cancelProgressDialog();
    }
  }

  void requestPayment(var data) async {
    showProgressDialog();
    /*var paymentData = {
      "DS_MERCHANT_AMOUNT": "145",
      "DS_MERCHANT_CURRENCY": "978",
      "DS_MERCHANT_CVV2": "123",
      "DS_MERCHANT_EXPIRYDATE": "1220",
      "DS_MERCHANT_MERCHANTCODE": "351374624",
      "DS_MERCHANT_ORDER": "1446068541",
      "DS_MERCHANT_PAN": "4548812049400004",
      "DS_MERCHANT_TERMINAL": "1",
      "DS_MERCHANT_TRANSACTIONTYPE": "0"
    };
    try {
      Codec<String, String> stringToBase64 = utf8.fuse(base64);
      String json  = jsonEncode(paymentData);
      String encoded = stringToBase64.encode(json);
      String key = stringToBase64.encode('sq7HjrUOBfKmC576ILgskD5srU870gJ7');
      var tripleDes = new BlockCipher(new TripleDESEngine(), key);
      String key2 = tripleDes.encodeB64(paymentData['DS_MERCHANT_ORDER']);
      var hmacSha256 = new Hmac(sha256, key2.codeUnits); //
      var digest = hmacSha256.convert(encoded.codeUnits);

      var body = {
        "Ds_MerchantParameters": encoded,
        'Ds_Signature': base64.encode(digest.bytes),
        "Ds_SignatureVersion": 'HMAC_SHA256_V1'
      };*/
    Map<String, String> headers = {
      'Content-Type': 'application/json',
    };
    try {
      final response = await http
          .post(ApiURL.apiUrlPosPayment,
              headers: headers, body: data)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handlePaymentResponse(response.body);
      } else {
        handlePaymentResponse("");
      }
    } catch (e) {
      handlePaymentResponse("");
    }
  }

  void handlePaymentResponse(var response) async {
    cancelProgressDialog();
    if (response != "") {
      print(response);
      Map<String, dynamic> map = json.decode(response);
      if (map['status'] == "completed") {
        isDone = true;
        successTicket(map);
      }
      if (map['error_code'] != null) {
        showInSnackBar(map['description']);
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  Future<bool> onWillPop1() async {
    if (isDone) {
      Navigator.pop(context, true);
    } else {
      if (!await MyDialog.showNativePopUpWith2Buttons(context, 'Advertencia',
          '¿Desea cancelar la operacion?', 'Salir', 'Continuar')) {
        Navigator.pop(context, false);
      }
    }
  }

  successTicket(var payment) async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(16.0),
                      child: Material(
                        clipBehavior: Clip.antiAlias,
                        elevation: 2.0,
                        borderRadius: BorderRadius.circular(4.0),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              ProfileTile(
                                title: "Gracias!",
                                textColor: primaryColor,
                                subtitle: "Su Pago fue procesado!",
                              ),
                              ListTile(
                                title: Text(userOrder.order.first.name),
                              ),
                              ListTile(
                                title: Text("Fecha"),
                                subtitle: Text(
                                    new DateFormat("dd/MM/yyyy hh:mm a")
                                        .format(new DateTime.now())),
                              ),
                              ListTile(
                                title: Text("Monto"),
                                subtitle: Text(
                                    "\$" + userOrder.totalAmount.toString()),
                                trailing: Text("Completado"),
                              ),
                              Card(
                                clipBehavior: Clip.antiAlias,
                                elevation: 0.0,
                                color: Colors.grey.shade300,
                                child: ListTile(
                                  leading: Image(
                                    image: AssetImage("assets/openpay.png"),
                                    width: 80,
                                  ),
                                  title: Text(payment['card']['type']
                                          .toString()
                                          .substring(0, 1)
                                          .toUpperCase() +
                                      payment['card']['type']
                                          .toString()
                                          .substring(1)),
                                  subtitle: Text(payment['card']['brand']
                                          .toString()
                                          .toUpperCase() +
                                      " ***" +
                                      payment['card']['card_number']
                                          .toString()
                                          .substring(payment['card']
                                                      ['card_number']
                                                  .toString()
                                                  .length -
                                              1)),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    FloatingActionButton(
                      backgroundColor: Colors.black,
                      child: Icon(
                        Icons.clear,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.pop(context, true);
                        onWillPop1();
                      },
                    )
                  ],
                ),
              ),
            ));
  }
}

class ProfileTile extends StatelessWidget {
  final title;
  final subtitle;
  final textColor;

  ProfileTile({this.title, this.subtitle, this.textColor = Colors.black});

  @override
  Widget build(BuildContext context) {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
              fontSize: 20.0, fontWeight: FontWeight.w700, color: textColor),
        ),
        SizedBox(
          height: 5.0,
        ),
        Text(
          subtitle,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.normal, color: textColor),
        ),
      ],
    );
  }
}
