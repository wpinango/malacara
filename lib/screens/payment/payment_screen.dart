import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:malacara/screens/payment/pos_webview_screen.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/TextFields/input_field.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/daos/order_dao.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/restaurant.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/models/user_order.dart';
import 'package:malacara/screens/payment/webview_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../api_url.dart';
import '../../global.dart';
import '../../routes.dart';
import '../../theme.dart';
import '../../util.dart';

class PaymentScreen extends StatefulWidget {
  final Restaurant restaurant;
  final List<Product> products;
  final UserOrder userOrder;

  const PaymentScreen({Key key, this.restaurant, this.products, this.userOrder})
      : super(key: key);

  @override
  PaymentScreenState createState() => new PaymentScreenState(
      products: products, restaurant: restaurant, userOrder: userOrder);
}

class PaymentScreenState extends State<PaymentScreen> {
  BuildContext context;
  var timeout = const Duration(seconds: 100);
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  Restaurant restaurant;
  List<Product> products;
  UserOrder userOrder;
  Country selectedCountry;
  User user;
  var f;
  String paymentType = 'openpay';
  String openpayType = 'openpay';
  String paypalType = 'paypal';
  bool isPaypalDone = false;
  bool isOpenPayDone = false;

  @override
  initState() {
    super.initState();
    initVars();
  }

  void initVars() async {
    final prefs = await SharedPreferences.getInstance();
    selectedCountry =
        Country.fromJson(json.decode(prefs.get('selectedCountry')));
    setState(() {
      f = Global.getCurrencySymbol(selectedCountry);
    });
    user = User.fromJson(json.decode(prefs.get('user')));
  }

  PaymentScreenState({this.products, this.restaurant, this.userOrder}) {
    print(makeIdReference());
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      backgroundColor: Color(0xfff8f9fa),
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Pago del pedido',
          style: new TextStyle(
            color: secondaryColor,
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                //controller: scrollController,
                child: new Container(
                  child: new Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(20),
                        //padding: EdgeInsets.all(8),
                        child: Text(
                          restaurant.name,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.varelaRound(
                              textStyle: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      ),
                      getShoppingDetails(screenSize),
                      Card(
                        margin:
                            EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                        child: Container(
                            padding: EdgeInsets.all(8),
                            child: Column(
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('Subtotal:',
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold))),
                                          Text(
                                              f.format(
                                                  Product.getSubtotal(products)),
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontWeight: FontWeight.bold)))
                                        ],
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('Transporte:',
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold))),
                                          Text(
                                              f.format(double.parse(
                                                  restaurant.transport)),
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontWeight: FontWeight.bold)))
                                        ],
                                      ),
                                    ),
                                    new Divider(
                                      color: Colors.grey,
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            'Total:',
                                            style: GoogleFonts.varelaRound(
                                                textStyle: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.bold)),
                                          ),
                                          Text(
                                              f.format(
                                                  Product.getSubtotal(products) +
                                                      double.parse(
                                                          restaurant.transport)),
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold))),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      Text('Descuentos disponibles'),
                      Card(
                        margin: EdgeInsets.all(8),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          width: screenSize.width / 1,
                          //margin: EdgeInsets.all(8),
                          padding: EdgeInsets.all(16),
                          child: Text(
                            'Sin descuentos disponibles',
                            style: GoogleFonts.varelaRound(
                                textStyle: TextStyle(fontWeight: FontWeight.bold)),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      Text('Cupones descuento',
                          style: GoogleFonts.varelaRound(textStyle: TextStyle())),
                      Card(
                        margin: EdgeInsets.all(8),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              width: 2,
                              color: nexiBlue,
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          //padding: EdgeInsets.all(8),
                          child: new InputField(
                              hintText: 'validar tu cupon de descuento',
                              obscureText: false,
                              textInputType: TextInputType.emailAddress,
                              textStyle: textStyle,
                              //hintStyle: hintStyle,
                              textCapitalization: TextCapitalization.none,
                              textFieldColor: white,
                              icon: Icons.local_offer,
                              iconColor: const Color(0xFF386080),
                              bottomMargin: 2,
                              //validateFunction: validations.validateEmail,
                              onSaved: (String discount) {
                                userOrder.discount = discount;
                                //credentials.user = email;
                              }),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      Text('Formas de pago',
                          style: GoogleFonts.varelaRound(textStyle: TextStyle())),
                      Card(
                        margin:
                            EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                        child: Container(
                            padding: EdgeInsets.all(8),
                            child: Column(
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      paymentType = openpayType;
                                    });
                                  },
                                  child: Container(
                                      width: screenSize.width / 1.1,
                                      margin: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Row(
                                            children: [
                                              //Image(image: AssetImage("assets/openpay.png"), width: 40,),
                                              Padding(padding: EdgeInsets.all(2),),
                                              Text('Pago con tarjeta',
                                                  style: GoogleFonts.varelaRound(
                                                      textStyle: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold))),
                                            ],
                                          ),
                                          paymentType == openpayType
                                              ? Icon((Icons.check))
                                              : Icon(
                                                  (Icons.check),
                                                  color: white,
                                                )
                                        ].where((child) => child != null).toList(),
                                      )),
                                ),
                                new Divider(
                                  color: Colors.grey,
                                ),
                                //Text('POS Movil (pago presencial)'),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      paymentType = paypalType;
                                    });
                                  },
                                  child: Container(
                                      width: screenSize.width / 1.1,
                                      margin: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Row(
                                            children: [
                                              //Image(image: AssetImage("assets/paypal.png"), width: 40,),
                                              Text(
                                                  isPaypalDone
                                                      ? 'Paypal (Pagado)'
                                                      : 'Paypal',
                                                  style: GoogleFonts.varelaRound(
                                                      textStyle: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold))),
                                            ],
                                          ),
                                          paymentType == paypalType
                                              ? Icon((Icons.check))
                                              : Icon(
                                                  (Icons.check),
                                                  color: white,
                                                )
                                        ].where((child) => child != null).toList(),
                                      )),
                                ),
                              ],
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      Text('Comentarions (opcionales)',
                          style: GoogleFonts.varelaRound(textStyle: TextStyle())),
                      Card(
                        margin:
                            EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                        child: Container(
                          padding: EdgeInsets.all(8),
                          child: new InputField(
                              hintText: 'Escribe tus comentarios',
                              obscureText: false,
                              maxLines: 3,
                              textInputType: TextInputType.multiline,
                              textStyle: textStyle,
                              hintStyle:
                                  GoogleFonts.varelaRound(textStyle: TextStyle()),
                              textCapitalization: TextCapitalization.none,
                              textFieldColor: textFieldColor,
                              iconColor: const Color(0xFF386080),
                              bottomMargin: 2,
                              onSaved: (String comment) {
                                userOrder.orderClientInfo.comment = comment;
                              }),
                        ),
                      ),
                      RoundedButton(
                        buttonName: 'Finalizar pedido',
                        onTap: onButtonPressed,
                        width: screenSize.width / 1.1,
                        height: screenSize.height / 13,
                        bottomMargin: 10.0,
                        borderWidth: 1.0,
                        buttonColor: secondaryColor,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  void onButtonPressed() async {
    if (await MyDialog.showNativePopUpWith2Buttons(
        context, 'Alerta', 'Desea realizar pedido?', 'Cancelar', 'Aceptar')) {
      if (userOrder.orderClientInfo.date == null ||
          userOrder.orderClientInfo.date == '') {
        userOrder.orderClientInfo.date =
            new DateFormat("yyyy/MM/dd+HH:mm:ss").format(new DateTime.now());
      }
      userOrder.reference = makeIdReference();
      userOrder.totalAmount =
          Restaurant.getTotalRestaurantAmount(restaurant, products);
      print(userOrder.totalAmount.toString());
      userOrder.discount = '';
      userOrder.token = user.token;
      userOrder.scrapped = Util.generateMd5(userOrder.token +
          userOrder.reference +
          format(userOrder.totalAmount) +
          userOrder.orderClientInfo.date);
      if ((paymentType == paypalType && isPaypalDone) || (paymentType == openpayType && isOpenPayDone)) {
        requestOrder();
      } else if (paymentType == paypalType && !isPaypalDone) {
        openWebViewScreen();
      } else if (paymentType == openpayType && !isOpenPayDone) {
        openOpenPayScreen();
      }
    }
  }

  void openOpenPayScreen() async {
    userOrder.orderClientInfo.paymentWayId = 15; //OPENPAY
    if (await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new PosWebViewScreen(
                  userOrder: userOrder,
                  //currencyCode: f.currencyName,
                )))) {
      requestOrder();
      isOpenPayDone = true;
    } else {
      showInSnackBar('No se pudo completar el pago con tarjeta');
    }
  }

  void openWebViewScreen() async {
    userOrder.orderClientInfo.paymentWayId = 1; //PAYPAL
    if (await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new WebViewScreen(
                  userOrder: userOrder,
                  currencyCode: f.currencyName,
                )))) {
      requestOrder();
      isPaypalDone = true;
    } else {
      showInSnackBar('No se pudo completar el pago en Paypal');
    }
  }

  Widget getShoppingDetails(Size screenSize) {
    List<Widget> list = new List();
    for (int index = 0; index < products.length; index++) {
      var product = products[index];
      list.add(Card(
        margin: EdgeInsets.all(8),
        child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Color(0xffe2e2e2),
              ),
              borderRadius: BorderRadius.circular(5),
            ),
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      //padding: EdgeInsets.all(8),
                      width: screenSize.width / 2,
                      child: Text(
                        product.name,
                        style: GoogleFonts.varelaRound(
                            textStyle: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      child: Text(
                        f.format(Product.getProductAmount(product)),
                        style: GoogleFonts.varelaRound(
                            textStyle: TextStyle(fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ),
                product.ingredients.length > 0
                    ? Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 4, bottom: 2, top: 2),
                            child: Text(
                              'No quiero: ',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 0, bottom: 2, top: 2),
                            child: Text(
                              Product.getRemoveIngredients(product, f),
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ],
                      )
                    : null,
                product.extras.length > 0
                    ? Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 4, bottom: 2),
                            child: Text(
                              'Extras: ',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            width: screenSize.width / 1.5,
                            margin: EdgeInsets.only(left: 2, bottom: 2),
                            child: Text(
                              Product.getExtras(product, f),
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ],
                      )
                    : null,
                product.combinations.length > 0
                    ? Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 4, bottom: 2),
                            child: Text(
                              'Opciones: \n',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            width: screenSize.width / 1.5,
                            margin: EdgeInsets.only(left: 2, bottom: 2),
                            child: Text(
                              Product.getCombinations(product, f),
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ],
                      )
                    : null,
              ].where((child) => child != null).toList(),
            )),
      ));
    }
    return new Column(
      children: list,
    );
  }

  void requestOrder() async {
    showProgressDialog();
    try {
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      String url = ApiURL.apiURL; //prefs.get(Global.keyApiUrl);
      if (userOrder.orderClientInfo.date == null ||
          userOrder.orderClientInfo.date == '') {
        userOrder.orderClientInfo.date =
            new DateFormat("yyyy/MM/dd+HH:mm:ss").format(new DateTime.now());
      }
      userOrder.reference = makeIdReference();
      userOrder.totalAmount =
          Restaurant.getTotalRestaurantAmount(restaurant, products);
      userOrder.discount = '';
      userOrder.token = user.token;
      userOrder.scrapped = Util.generateMd5(userOrder.token +
          userOrder.reference +
          (format(userOrder.totalAmount)) +
          userOrder.orderClientInfo.date);
      userOrder.orderClientInfo.paymentWayId = paymentType == 'paypal' ? 1 : 3;
      var body = userOrder;
      Map<String, String> map = {
        'datos': jsonEncode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post(url + ApiURL.urlSetOrder.replaceAll('?', apiVersion),
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleOrderResponse(response.body);
      } else {
        handleOrderResponse("");
      }
    } catch (e) {
      handleOrderResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleOrderResponse(final responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      if (map['resultado']['code'] == 0) {
        OrderDao orderDao = new OrderDao();
        orderDao.insertOrder(userOrder);
        onPressed(context, '/PaymentSuccess');
      } else {
        cancelProgressDialog();
        showInSnackBar('Algo salio mal');
      }
    }
  }

  String makeIdReference() {
    var fullDate = new DateTime.now();
    var year = fullDate.year.toString();
    var month = fullDate.month.toString();
    if ((fullDate.month + 1) < 10) month = '0' + fullDate.month.toString();
    var dia = fullDate.day.toString();
    if ((fullDate.day) < 10) dia = '0' + fullDate.day.toString();
    var hora = fullDate.hour.toString();
    if ((fullDate.hour) < 10) hora = '0' + fullDate.hour.toString();
    var minutes = fullDate.minute.toString();
    if ((fullDate.minute) < 10) minutes = '0' + fullDate.minute.toString();
    var seconds = fullDate.second.toString();
    if ((fullDate.second) < 10) seconds = '0' + fullDate.second.toString();
    year = year.substring(2, 4);
    return year +
        '' +
        month +
        '' +
        dia +
        '' +
        hora +
        '' +
        minutes +
        '' +
        seconds;
  }

  onPressed(BuildContext context, String routeName) {
    Routes.replacementScreen(context, routeName);
  }

  String format(double n) {
    if (n.truncateToDouble() == n) {
      return n.toStringAsFixed(0);
    } else {
      return n.toString();
    }
    //return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 2);
  }
}
