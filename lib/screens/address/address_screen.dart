import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/components/zoombuttons_plugin_option.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/address.dart' as userAddress;
import 'package:malacara/models/country.dart';
import 'package:malacara/models/credentials.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/models/user_location.dart';
import 'package:malacara/service/validations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:latlong/latlong.dart';

import '../../api_url.dart';
import '../../global.dart';
import '../../theme.dart';
import '../../util.dart';

class AddressScreen extends StatefulWidget {
  final isOder;

  const AddressScreen({Key key, this.isOder}) : super(key: key);

  @override
  AddressScreenState createState() => new AddressScreenState(isOder);
}

const kGoogleApiKey = 'AIzaSyDZZSgj5F6EDyoSwReFKk9DJ3K9a6S4IeM';
GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class AddressScreenState extends State<AddressScreen> {
  User user;
  userAddress.Address selectedAddress;
  userAddress.Address newAddress = new userAddress.Address();
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  ScrollController scrollController = new ScrollController();
  var timeout = const Duration(seconds: 10);
  bool isInAsyncCall = false;
  List<userAddress.Address> values = new List();
  bool autoValidate = false;
  Country selectedCountry = new Country();
  UserLocation userLocation;
  TextEditingController countryController = new TextEditingController();
  TextEditingController infoController = new TextEditingController();
  Validations validations = new Validations();
  bool isNew = false;
  LatLng latLng;
  var markers;
  bool isOrder = false;

  AddressScreenState(bool isOrder) {
    if (isOrder != null) {
      this.isOrder = isOrder;
    }
  }

  @override
  initState() {
    super.initState();
    initVars();
  }

  void initVars() async {
    final prefs = await SharedPreferences.getInstance();
    user = User.fromJson(json.decode(prefs.get('user')));
    values.clear();
    for (var a in user.address) {
      userAddress.Address address = new userAddress.Address();
      address.address = a['address'];
      address.addressId = a['id_direccion'];
      address.cityId = a['id_city'];
      address.lat = a['lat'];
      address.lng = a['lng'];
      address.info = a['info'];
      values.add(address);
    }
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        selectedCountry =
            Country.fromJson(json.decode(prefs.get('selectedCountry')));
      });
    }
    setState(() {});
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void saveAddress() async {
    showProgressDialog();
    newAddress.address = countryController.text;
    newAddress.lat = userLocation.latitude.toString();
    newAddress.lng = userLocation.longitude.toString();
    newAddress.addressId = '-1';

    var body = {
      'address': newAddress.address,
      'lat': newAddress.lat,
      'lng': newAddress.lng,
      'postal_code': '',
      'info': newAddress.info,
      'accion': 1,
      'id_direccion': '-1',
      'token': user.token,
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    try {
      final response = await http
          .post(
              ApiURL.apiURL +
                  ApiURL.urlSetDirection.replaceAll('?', apiVersion),
              headers: headers,
              body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleSaveAddressResponse(response.body);
      } else {
        handleSaveAddressResponse("");
      }
    } catch (e) {
      handleSaveAddressResponse("");
    }
  }

  void handleSaveAddressResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        countryController.text = '';
        infoController.text = '';
        selectedAddress = null;
        latLng = null;
        setState(() {});
        refreshData();
        newAddress.cityId = map['resultado']['id_ciudad'];
        newAddress.addressId = map['resultado']['id'].toString();
        if(!isOrder) {
          await MyDialog.showNativePopUpWith2Buttons(
              context, 'Direccion añadida',
              'Su direccion fue agregada correctamente', 'Cerrar', 'Aceptar');
        }
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('El usuario o contraseña son incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio temporalmente no disponiblefalse');
      } else if (map['resultado']['code'] == -5) {
        showInSnackBar('Sesion iniciada en otro dispositivo');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  void returnToOrder(var map) {
    if (isOrder) {
      Navigator.pop(context, newAddress);
    }
  }

  void removeAddress() async {
    isOrder = false;
    showProgressDialog();
    var body = {
      'address': selectedAddress.address,
      'lat': selectedAddress.lat,
      'lng': selectedAddress.lng,
      'postal_code': '',
      'info': selectedAddress.info,
      'accion': 2,
      'id_direccion': selectedAddress.addressId,
      'token': user.token,
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    try {
      final response = await http
          .post(
              ApiURL.apiURL +
                  ApiURL.urlSetDirection.replaceAll('?', apiVersion),
              headers: headers,
              body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleRemoveAddressResponse(response.body);
      } else {
        handleRemoveAddressResponse("");
      }
    } catch (e) {
      handleRemoveAddressResponse("");
    }
  }

  void handleRemoveAddressResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        for (int i = 0; i < user.address.length; i++) {
          if (user.address[i]['id_direccion'] == selectedAddress.addressId) {
            user.address.removeAt(i);
          }
        }
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('user', json.encode(user));
        countryController.text = '';
        infoController.text = '';
        Navigator.pop(context, null);
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('El usuario o contraseña son incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio temporalmente no disponible');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  void refreshData() async {
    showProgressDialog();
    final prefs = await SharedPreferences.getInstance();
    Credentials credentials =
        Credentials.fromJson(json.decode(prefs.get(Global.keyCredentials)));
    print(credentials.user);
    var body = {
      'username': credentials.user,
      'password': Util.generateMd5(credentials.password),
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    String apiVersion = prefs.get('apiVersion');
    try {
      final response = await http
          .post(ApiURL.apiURL + ApiURL.doLogin.replaceAll('?', apiVersion),
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleRefreshDataResponse(response.body);
      } else {
        handleRefreshDataResponse("");
      }
    } catch (e) {
      handleRefreshDataResponse("");
    }
  }

  void handleRefreshDataResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        print(json.encode(map['resultado']));
        final prefs = await SharedPreferences.getInstance();
        user = User.fromJson(json.decode(json.encode(map['resultado'])));
        prefs.setString('user', json.encode(user));
        prefs.setBool('login', true);
        initVars();
        print('primer token: ' + user.token);
        setState(() {
        });
        if (isOrder) {
          if (await MyDialog.showNativePopUpWith2Buttons(
              context,
              'Direccion añadida',
              'Su direccion fue agregada correctamente',
              'Cerrar',
              'Aceptar')) {
            returnToOrder(map);
          } else {
            returnToOrder(map);
          }
        }
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('El usuario o contraseña son incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio temporalmente no disponible');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Mis direccioness',
          style: new TextStyle(
            color: secondaryColor,
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, true)),
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 8, left: 8),
                        alignment: Alignment.centerLeft,
                        child: Text('Mis direcciones'),
                      ),
                      Container(
                          margin: new EdgeInsets.only(
                              bottom: 8.0, top: 8.0, right: 8, left: 8),
                          width: screenSize.width / 1.2,
                          height: screenSize.height / 14,
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.circular(5.0),
                            border:
                                Border.all(style: BorderStyle.solid, width: 0.80),
                          ),
                          child: new DropdownButtonHideUnderline(
                            child: DropdownButton<userAddress.Address>(
                              isExpanded: true,
                              hint: Text(
                                'Selecciona una direccion',
                                maxLines: 1,
                                softWrap: false,
                                overflow: TextOverflow.ellipsis,
                              ),
                              items: values
                                  .map<DropdownMenuItem<userAddress.Address>>(
                                      (userAddress.Address value) {
                                return DropdownMenuItem<userAddress.Address>(
                                  value: value,
                                  child: Text(
                                    value.address,
                                    maxLines: 1,
                                    softWrap: false,
                                    style: GoogleFonts.varelaRound(),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                              onChanged: (userAddress.Address value) {
                                setState(() {
                                  isNew = false;
                                  selectedAddress = value;
                                  countryController.text = selectedAddress.address;
                                  infoController.text = selectedAddress.info;
                                  latLng = null;
                                  findCoordinatesFromAddress(value.address);
                                });
                              },
                              value: selectedAddress,
                              isDense: true,
                            ),
                          )),
                      Padding(padding: EdgeInsets.all(8)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new SizedBox(
                              width: screenSize.width / 2.5,
                              child: OutlineButton.icon(
                                  onPressed: () {
                                    isNew = true;
                                    onSearchAddressPressed();
                                  },
                                  shape: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(4))),
                                  icon: new Icon(
                                    Icons.add,
                                  ),
                                  label: Text(
                                    'Nueva',
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ))),
                          new SizedBox(
                              width: screenSize.width / 2.5,
                              child: OutlineButton.icon(
                                  onPressed: () {
                                    showDeleteAddressDialog();
                                  },
                                  shape: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(4))),
                                  icon: new Icon(
                                    Icons.delete,
                                  ),
                                  label: Text(
                                    'Eliminar',
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ))),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.all(8),
                        child: new Form(
                            key: formKey,
                            autovalidate: autoValidate,
                            child: new Column(
                              children: <Widget>[
                                new TextFormField(
                                  decoration:
                                      const InputDecoration(labelText: 'Direccion'),
                                  keyboardType: TextInputType.text,
                                  controller: countryController,
                                  validator: validations.validateTextInput,
                                  enabled: false,
                                  onSaved: (String value) {},
                                ),
                                new TextFormField(
                                  decoration: const InputDecoration(
                                      labelText: 'Detalle (piso, numero, casa)'),
                                  keyboardType: TextInputType.text,
                                  controller: infoController,
                                  onSaved: (String value) {
                                    newAddress.info = value;
                                  },
                                ),
                              ],
                            )),
                      ),
                      Padding(padding: EdgeInsets.all(8)),
                      latLng != null
                          ? Container(
                              height: screenSize.height / 3,
                              child: FlutterMap(
                                options: MapOptions(
                                    center: latLng,
                                    zoom: 17.0,
                                    plugins: [
                                      ZoomButtonsPlugin(),
                                    ],
                                    onTap: (LatLng point) {
                                      if (isNew) {
                                        findAddressFromCoordinates(point);
                                      }
                                    }),
                                layers: [
                                  TileLayerOptions(
                                    urlTemplate:
                                        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                                    subdomains: ['a', 'b', 'c'],
                                    tileProvider: NonCachingNetworkTileProvider(),
                                  ),
                                  MarkerLayerOptions(markers: markers),
                                  ZoomButtonsPluginOption(
                                      minZoom: 4,
                                      maxZoom: 19,
                                      mini: true,
                                      padding: 10,
                                      alignment: Alignment.bottomRight)
                                ],
                              ),
                            )
                          : null,
                      Padding(padding: EdgeInsets.all(8)),
                      RoundedButton(
                        buttonName: 'Crear direccion',
                        onTap: () {
                          if (isNew) {
                            handleFormSubmitted();
                          } else {
                            showInSnackBar(
                                'Debe pulsar el boton agregar, para añadir direccion');
                          }
                        },
                        width: screenSize.width / 1.1,
                        height: screenSize.height / 13,
                        bottomMargin: 10.0,
                        borderWidth: 1.0,
                        buttonColor: secondaryColor,
                      ),
                      Padding(padding: EdgeInsets.all(8)),
                    ].where((child) => child != null).toList(),
                  ),
                ),
              ),
            ),
          ],
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      //resizeToAvoidBottomPadding: false,
    );
  }

  void findAddressFromCoordinates(LatLng latLng) async {
    final coordinates = new Coordinates(latLng.latitude, latLng.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print("${first.featureName} : ${first.addressLine}");
    countryController.text = (first.addressLine);
    markers = <Marker>[
      Marker(
        width: 100.0,
        height: 100.0,
        point: latLng,
        builder: (ctx) => Container(
            child: Icon(
          Icons.location_on,
          color: Colors.redAccent,
          size: 45,
        )),
      ),
    ];
    setState(() {});
  }

  void findCoordinatesFromAddress(String address) async {
    var coordinates = await Geocoder.local.findAddressesFromQuery(address);
    setState(() {
      latLng = new LatLng(coordinates.first.coordinates.latitude,
          coordinates.first.coordinates.longitude);
      markers = <Marker>[
        Marker(
          width: 100.0,
          height: 100.0,
          point: latLng,
          builder: (ctx) => Container(
              child: Icon(
            Icons.location_on,
            color: Colors.redAccent,
            size: 45,
          )),
        ),
      ];
    });
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      saveAddress();
    }
  }

  void onSearchAddressPressed() async {
    infoController.text = '';
    Prediction p = await PlacesAutocomplete.show(
        context: context,
        apiKey: kGoogleApiKey,
        mode: Mode.fullscreen,
        logo: Text(''),
        hint: 'Direccion',
        language: selectedCountry.completeCode.split('-')[0],
        components: [new Component(Component.country, selectedCountry.code)]);
    PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
    userLocation = new UserLocation();
    userLocation.latitude = detail.result.geometry.location.lat;
    userLocation.longitude = detail.result.geometry.location.lng;
    userLocation.location = p.description;

    setState(() {
      latLng = new LatLng(userLocation.latitude, userLocation.longitude);
      markers = <Marker>[
        Marker(
          width: 100.0,
          height: 100.0,
          point: latLng,
          builder: (ctx) => Container(
              child: Icon(
            Icons.location_on,
            color: Colors.redAccent,
            size: 45,
          )),
        ),
      ];
    });
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('location', json.encode(userLocation));
    countryController.text = userLocation.location;
  }

  void showDeleteAddressDialog() async {
    if (selectedAddress != null) {
      if (await MyDialog.showNativePopUpWith2Buttons(
          context,
          'Eliminar direccion',
          'Desea realmente eliminar ',
          'Cancelar',
          'Eliminar')) {
        removeAddress();
      }
    } else {
      showInSnackBar('Debe seleccionar una direccion');
    }
  }
}
