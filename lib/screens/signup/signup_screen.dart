import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/TextFields/input_field.dart';
import 'package:malacara/components/TextFields/password_input_field.dart';
import 'package:malacara/components/TextFields/phone_input_field.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/models/credentials.dart';
import 'package:malacara/models/sign_up.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/service/validations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../api_url.dart';
import '../../global.dart';
import '../../routes.dart';
import '../../theme.dart';
import '../../util.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key key}) : super(key: key);

  @override
  SignUpScreenState createState() => new SignUpScreenState();
}

class SignUpScreenState extends State<SignUpScreen> {
  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  Validations validations = new Validations();
  SignUp signUp = new SignUp();
  bool isInAsyncCall = false;
  var timeout = const Duration(seconds: 15);
  bool isHidePass = true;
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  onPressed(String routeName) {
    Navigator.pop(context);
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void handleSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      requestSignUp();
    }
  }

  void requestSignUp() async {
    showProgressDialog();
    var body = {
      'username': signUp.username,
      'nombre': signUp.name,
      'apellidos': signUp.lastName,
      'movil': signUp.phone,
      'password': Util.generateMd5(signUp.password),
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    try {
      final response = await http
          .post(ApiURL.apiURL + ApiURL.urlRegister.replaceAll('?', apiVersion),
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleSignUpResponse(response.body);
      } else {
        handleSignUpResponse("");
      }
    } catch (e) {
      handleSignUpResponse("");
    }
  }

  handleSignUpResponse(final responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      if (map['resultado']['code'] == 0) {
        User user = new User(
            name: signUp.name,
            token: map['resultado']['token'],
            email: signUp.username,
            mobileNumber: signUp.phone,
            lastName: signUp.lastName);
        Credentials credentials =
            new Credentials(password: signUp.password, user: signUp.username);
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('user', json.encode(user));
        prefs.setBool('login', true);
        prefs.setString(Global.keyCredentials, json.encode(credentials));
        Routes.replacementScreen(context, "/FindRest");
      } else if (map['resultado']['code'] == -4) {
        showInSnackBar('Datos incompletos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Email de usuario repetido');
      }
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Container(
        alignment: Alignment.center,
        width: screenSize.width,
        padding: new EdgeInsets.all(8.0),
        decoration: new BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/bg-carta-comidas.jpg"),
              fit: BoxFit.cover),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: new AppBar(
              title: new Text(
                'REGISTRO',
                style: new TextStyle(
                  color: secondaryColor,
                  fontSize: Theme.of(context).platform == TargetPlatform.iOS
                      ? 17.0
                      : 20.0,
                ),
              ),
              actions: <Widget>[],
              backgroundColor: Colors.transparent,
              iconTheme: IconThemeData(color: secondaryColor),
              elevation:
                  0.0 //Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
              ),
          key: scaffoldKey,
          body: ModalProgressHUD(
            child: SingleChildScrollView(
                controller: scrollController,
                child: new Container(
                  width: screenSize.width,
                  padding: new EdgeInsets.only(left: 32, right: 32),
                  decoration: new BoxDecoration(
                  ),
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        width: screenSize.width / 1.4,
                        height: screenSize.height,
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              child: Image.asset('assets/logo.png'),
                              width: screenSize.width / 1.5,
                            ),
                            Padding(
                              padding: EdgeInsets.all(32),
                            ),
                            new Form(
                                key: formKey,
                                autovalidate: autoValidate,
                                child: new Column(
                                  children: <Widget>[
                                    new InputField(
                                        hintText: "Correo",
                                        obscureText: false,
                                        textInputType:
                                            TextInputType.emailAddress,
                                        textStyle: textStyle,
                                        textCapitalization:
                                            TextCapitalization.none,
                                        textFieldColor: textFieldColor,
                                        hintStyle: hintStyle,
                                        icon: Icons.mail_outline,
                                        iconColor: const Color(0xFF386080),
                                        bottomMargin: 20.0,
                                        validateFunction:
                                            validations.validateEmail,
                                        onSaved: (String email) {
                                          signUp.username = email;
                                        }),
                                    new InputField(
                                        hintText: "Nombre",
                                        obscureText: false,
                                        textInputType: TextInputType.text,
                                        textStyle: textStyle,
                                        hintStyle: hintStyle,
                                        textFieldColor: textFieldColor,
                                        icon: Icons.person_outline,
                                        iconColor: const Color(0xFF386080),
                                        bottomMargin: 20.0,
                                        textCapitalization:
                                            TextCapitalization.sentences,
                                        validateFunction:
                                            validations.validateLength,
                                        onSaved: (String name) {
                                          signUp.name = name;
                                        }),
                                    new InputField(
                                        hintText: "Apellidos",
                                        obscureText: false,
                                        textInputType: TextInputType.text,
                                        textStyle: textStyle,
                                        textFieldColor: textFieldColor,
                                        hintStyle: hintStyle,
                                        textCapitalization:
                                            TextCapitalization.sentences,
                                        icon: Icons.person_outline,
                                        iconColor: const Color(0xFF386080),
                                        bottomMargin: 20.0,
                                        validateFunction:
                                            validations.validateLength,
                                        onSaved: (String name) {
                                          signUp.lastName = name;
                                        }),
                                    new PhoneInputField(
                                        hintText: "Nº Telefono",
                                        obscureText: false,
                                        textInputType: TextInputType.phone,
                                        textStyle: textStyle,
                                        hintStyle: hintStyle,
                                        prefixText: '+',
                                        textFieldColor: textFieldColor,
                                        icon: Icons.phone,
                                        iconColor: const Color(0xFF386080),
                                        bottomMargin: 20.0,
                                        validateFunction:
                                            validations.validateTextInput,
                                        onSaved: (String phone) {
                                          signUp.phone = phone;
                                        }),
                                    new PasswordInputField(
                                      hintText: "Contraseña",
                                      obscureText: isHidePass,
                                      controller: passwordController,
                                      textInputType: TextInputType.text,
                                      textStyle: textStyle,
                                      hintStyle: hintStyle,
                                      textFieldColor: textFieldColor,
                                      icon: Icons.lock_open,
                                      iconColor: const Color(0xFF386080),
                                      bottomMargin: 20.0,
                                      validateFunction:
                                          validations.validatePassword,
                                      onSaved: (String password) {
                                        signUp.password = password;
                                      },
                                      suffixIcon: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            isHidePass = !isHidePass;
                                          });
                                        },
                                        child: Icon(!isHidePass
                                            ? Icons.visibility
                                            : Icons.visibility_off, color: secondaryColor,),
                                      ),
                                    ),
                                    new RoundedButton(
                                        buttonName: "Registrar",
                                        onTap: handleSubmitted,
                                        buttonColor: secondaryColor,
                                        width: screenSize.width,
                                        height: 50.0,
                                        bottomMargin: 5.0,
                                        borderWidth: 1.0),
                                  ],
                                )),
                          ],
                        ),
                      )
                    ],
                  ),
                )),
            inAsyncCall: isInAsyncCall,
            opacity: 0.5,
            progressIndicator: NativeLoadingIndicator(),
          ),
        ));
  }
}
