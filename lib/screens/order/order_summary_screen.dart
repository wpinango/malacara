import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/restaurant.dart';
import 'package:malacara/screens/order/order_address_screen.dart';
import 'package:malacara/screens/login/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../global.dart';
import '../../theme.dart';

class OrderSummaryScreen extends StatefulWidget {
  final Restaurant restaurant;
  final List<Product> products;

  const OrderSummaryScreen({Key key, this.restaurant, this.products})
      : super(key: key);

  @override
  OrderSummaryScreenState createState() => new OrderSummaryScreenState(
      products: this.products, restaurant: this.restaurant);
}

class OrderSummaryScreenState extends State<OrderSummaryScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  Restaurant restaurant;
  List<Product> products;
  Country selectedCountry;
  var f;

  OrderSummaryScreenState({this.products, this.restaurant});

  @override
  initState() {
    super.initState();
    getSelectedCounty();
  }

  void getSelectedCounty() async {
    final prefs = await SharedPreferences.getInstance();
    selectedCountry =
        Country.fromJson(json.decode(prefs.get('selectedCountry')));
    setState(() {
      f = Global.getCurrencySymbol(selectedCountry);
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      backgroundColor: Color(0xfff8f9fa),
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Resumen del pedido',
          style: new TextStyle(
            color: secondaryColor,
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  //height: screenSize.height * scrollFactor,
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(20),
                        child: Text(
                          restaurant.name,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.varelaRound(
                              textStyle: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold)),
                        ),
                      ),
                      getShoppingDetails(screenSize),
                      Card(
                        margin:
                            EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                        child: Container(
                            padding: EdgeInsets.all(8),
                            child: Column(
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.all(8),
                                      child: Text('Carrito de Compra',
                                          style: GoogleFonts.varelaRound(
                                              textStyle: TextStyle(
                                                  fontSize: 16,
                                                  color: terciaryColor,
                                                  fontWeight: FontWeight.bold))),
                                    ),
                                    getShoppingCar(screenSize),
                                    Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('Subtotal',
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold))),
                                          Text(
                                              f.format(
                                                  Product.getSubtotal(products)),
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontWeight: FontWeight.bold)))
                                        ],
                                      ),
                                    ),
                                    new Divider(
                                      color: Colors.grey,
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('Servicios de cobertura',
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                      FontWeight.bold))),
                                          Text('\$',
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontWeight: FontWeight.bold)))
                                        ],
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('Costo de envio',
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold))),
                                          Text(
                                              f.format(double.parse(
                                                  restaurant.transport.toString())),
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontWeight: FontWeight.bold)))
                                        ],
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('Descuento',
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                      FontWeight.bold))),
                                          Text(
                                              f.format(double.parse('0')),
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontWeight: FontWeight.bold)))
                                        ],
                                      ),
                                    ),
                                    /*Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('Propina',
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                      FontWeight.bold))),
                                          Text(
                                              f.format(double.parse(
                                                  restaurant.transport.toString())),
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontWeight: FontWeight.bold)))
                                        ],
                                      ),
                                    ),*/
                                  ],
                                ),
                                new Divider(
                                  color: Colors.grey,
                                ),
                                Container(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        'Total',
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Text(
                                          f.format(Product.getSubtotal(products) +
                                              double.parse(restaurant.transport.toString())),
                                          style: GoogleFonts.varelaRound(
                                              textStyle: TextStyle( color: secondaryColor,
                                                  fontWeight: FontWeight.bold))),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      RoundedButton(
                        buttonName: 'Realizar pedido',
                        onTap: onButtonPressed,
                        width: screenSize.width / 1.1,
                        height: screenSize.height / 13,
                        bottomMargin: 10.0,
                        borderWidth: 1.0,
                        buttonColor: secondaryColor,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  Widget getShoppingDetails(Size screenSize) {
    List<Widget> list = new List();
    for (int index = 0; index < products.length; index++) {
      var product = products[index];
      list.add(Card(
        margin: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
        child: Container(
            padding: EdgeInsets.all(4),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8),
                      width: screenSize.width / 1.5,
                      child: Text(
                        product.name +
                            ' (' +
                            f.format(double.parse(product.amount)) +
                            ')',
                        style: GoogleFonts.varelaRound(
                            textStyle: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                      ),
                    ),
                    Text(f.format(Product.getProductAmount(product)),
                        style: GoogleFonts.varelaRound(
                            textStyle: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)))
                  ],
                ),
                product.ingredients.length > 0
                    ? Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 4, bottom: 2, top: 2),
                            child: Text(
                              'No quiero: ',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 0, bottom: 2, top: 2),
                            child: Text(
                              Product.getRemoveIngredients(product, f),
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ],
                      )
                    : null,
                product.extras.length > 0
                    ? Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 4, bottom: 2),
                            child: Text(
                              'Extras: ',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            width: screenSize.width / 1.5,
                            margin: EdgeInsets.only(left: 2, bottom: 2),
                            child: Text(
                              Product.getExtras(product, f),
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ],
                      )
                    : null,
                product.combinations.length > 0
                    ? Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 4, bottom: 2),
                            child: Text(
                              'Opciones: \n',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            width: screenSize.width / 1.8,
                            margin: EdgeInsets.only(left: 2, bottom: 2),
                            child: Text(
                              Product.getCombinations(product, f),
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ],
                      )
                    : null,
                new Divider(
                  color: Colors.grey,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        IconButton(
                          icon: Image.asset('assets/circle-more.png'),
                          onPressed: () => {
                            setState(() {
                              products[index].quantity -= 1;
                            })
                          },
                        ),
                        Text(product.quantity.toString()),
                        IconButton(
                          icon: Image.asset('assets/circle-minus.png'),
                          onPressed: () => {
                            setState(() {
                              products[index].quantity += 1;
                            })
                          },
                        ),
                      ],
                    ),
                    IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        setState(() {
                          products.removeAt(index);
                        });
                      },
                    )
                  ],
                ),
              ].where((child) => child != null).toList(),
            )),
      ));
    }
    return new Column(
      children: list,
    );
  }

  Widget getShoppingCar(Size screenSize) {
    List<Widget> list = new List();
    int index = 0;
    for (var product in products) {
      index++;
      list.add(Container(
        margin: EdgeInsets.all(16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              index.toString(),
              style: GoogleFonts.varelaRound(
                  textStyle: TextStyle(fontWeight: FontWeight.bold)),
            ),
            Container(
              width: screenSize.width / 2,
              child: Text(
                product.name,
                style: GoogleFonts.varelaRound(
                    textStyle: TextStyle(fontWeight: FontWeight.bold)),
              ),
            ),
            Text(
              f.format(double.parse(product.amount) * product.quantity),
              style: GoogleFonts.varelaRound(
                  textStyle: TextStyle(fontWeight: FontWeight.bold)),
            )
          ],
        ),
      ));
      list.add(new Divider(
        color: Colors.grey,
      ));
    }
    return new Column(
      children: list,
    );
  }

  void onButtonPressed() async {
    try {
      int result = -1;
      bool isLogin = false;
      if (Product.getTotalAmount(products) > 0) {
        final prefs = await SharedPreferences.getInstance();
        isLogin = prefs.getBool('login');
        if (isLogin != null && isLogin) {
          openOrderAddressScreen();
        } else {
          result = await Navigator.push(context,
              new MaterialPageRoute(builder: (context) => new LoginScreen(isOnlyFinish: true,)));
          if (result == 1) {
            openOrderAddressScreen();
          }
        }
      }
    } catch (e) {
      e.toString();
    }
  }

  void openOrderAddressScreen() async {
    try {
      bool isSchedule = false;
      restaurant.schedules.forEach((key, value) {
        if (value.length > 0) {
          isSchedule = true;
        }
      });
      if (isSchedule) {
        await Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) =>
                new OrderAddressScreen(
                  restaurant: restaurant,
                  products: products,
                )));
      } else {
        showInSnackBar('El restaurante no tiene horarios asignados');
      }
    } catch (e) {
      e.toString();
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }
}
