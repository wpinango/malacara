import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:malacara/daos/order_dao.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/models/user_order.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../global.dart';
import '../../theme.dart';

class OrderHistoryScreen extends StatefulWidget {

  const OrderHistoryScreen({Key key}) : super(key: key);

  @override
  OrderHistoryScreenState createState() => new OrderHistoryScreenState();
}

class OrderHistoryScreenState extends State<OrderHistoryScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  var f;
  OrderDao orderDao;
  List<UserOrder> userOrders = new List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initVars();
  }

  initVars() async {
    orderDao = new OrderDao();
    List a = await orderDao.getAllOrders();
    final prefs = await SharedPreferences.getInstance();
    User user = User.fromJson(json.decode(prefs.get('user')));
    if (userOrders.isEmpty) {}
    for (var order in user.orders['info']) {
      order['nombre'] = order['negocio'];
      // order['productos'] = [order['prodcutos']];
      UserOrder userOrder = new UserOrder();
      userOrder.reference = order['referencia'];
      userOrder.totalAmount = double.parse(order['total']);
      userOrder.orderClientInfo = new OrderClientInfo();
      userOrder.orderClientInfo.date = order['fecha_registro'];
      userOrder.order = new List();
      userOrder.order.add(order);
      userOrders.add(userOrder);
    }
    Country selectedCountry =
    Country.fromJson(json.decode(prefs.get('selectedCountry')));
    setState(() {
      f = Global.getCurrencySymbol(selectedCountry);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Size size = MediaQuery.of(context).size;
    this.context = context;
    return Scaffold(
      backgroundColor: Color(0xfff8f9fa),
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Mis Pedidos',
          style: new TextStyle(
            color: secondaryColor,
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[
          /*IconButton(
            icon: Icon(Icons.delete),
            onPressed: () {
              shoWDeleteDialog(context);
            },
          )*/
        ],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: Container(
        child: userOrders.length > 0
            ? ListView.builder(
            itemCount: userOrders.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(child: getOrders(userOrders[index], size));
            })
            : Container(
          margin: EdgeInsets.all(48),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: size.width,
                height: size.height / 10,
                child: Icon(
                  Icons.shopping_basket,
                  size: 64,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(16),
              ),
              Text(
                'No tiene pedidos',
                style: GoogleFonts.varelaRound(
                    textStyle: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 20)),
              )
            ],
          ),
        ),
      ),
    );
  } //yyyy/MM/dd+HH:mm:ss

  void shoWDeleteDialog(BuildContext context) async {
    if (await MyDialog.showNativePopUpWith2Buttons(context, 'Alerta',
        '¿Desea borrar todos los pedidos?', 'Cancelar', 'Aceptar')) {
      orderDao.delete(userOrders.first);
      userOrders.clear();
      setState(() {});
    }
  }

  Widget getOrders(UserOrder userOrder, Size size) {
    List<Widget> widgets = new List();
    Widget totalAmount;
    List<Widget> cards = new List();
    for (var order in userOrder.order) {
      widgets.add(Text(
        order['nombre'],
        style: GoogleFonts.varelaRound(
            textStyle: TextStyle(fontWeight: FontWeight.bold)),
      ));
      try {
        var parsedDate = new DateFormat("dd/MM/yyyy HH:mm")
            .parse(userOrder.orderClientInfo.date.replaceAll('+', ' '));
        widgets.add(Text(
          timeago.format(parsedDate, locale: 'es'),
          style: GoogleFonts.varelaRound(),
        ));
      } catch (e) {
        e.toString();
      }
      widgets.add(Container(
        width: size.width / 2,
        child: Text(
          order['productos'],
          style: GoogleFonts.varelaRound(),
        ),
      ));
      /*for (var product in order['productos']) {
        widgets.add(Text(
          product['nombre'],
          style: GoogleFonts.varelaRound(),
        ));
      }*/
      totalAmount = new Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.all(8),
        width: size.width / 1.1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widgets,
            ),
            new Text(
              f.format(userOrder.totalAmount),
              style: GoogleFonts.varelaRound(
                  textStyle: TextStyle(fontWeight: FontWeight.bold)),
            )
          ],
        ),
      );
      cards.add(GestureDetector(
        child: Card(
            margin: EdgeInsets.only(left: 16, right: 16, top: 8),
            child: totalAmount),
        onTap: () {
          try {
            openDeliveryTrack(order);
          } catch(e) {
            e.toString();
          }
        },
      ));
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: cards,
    );
  }

  void openDeliveryTrack(var order) {
    //Navigator.push(context,
    //    new MaterialPageRoute(builder: (context) => new TrackOrderScreen(order: order,)));
  }
}

