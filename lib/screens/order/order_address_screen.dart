import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/components/input_dropdown.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/address.dart';
import 'package:malacara/models/restaurant.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/models/user_order.dart';
import 'package:malacara/routes.dart';
import 'package:malacara/screens/address/address_screen.dart';
import 'package:malacara/screens/payment/payment_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:http/http.dart' as http;

import '../../api_url.dart';
import '../../global.dart';
import '../../theme.dart';

class OrderAddressScreen extends StatefulWidget {
  final Restaurant restaurant;
  final List<Product> products;

  const OrderAddressScreen({
    Key key,
    this.restaurant,
    this.products,
  }) : super(key: key);

  @override
  OrderAddressScreenState createState() => new OrderAddressScreenState(
        restaurant: restaurant,
        products: products,
      );
}

class OrderAddressScreenState extends State<OrderAddressScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  BuildContext context;
  Restaurant restaurant;
  List<Product> products;
  List<Address> values = new List();
  UserOrder userOrder = new UserOrder();
  Address address;
  DateTime nextAvailableDate;
  bool isEdit = false;
  DateTime selectedDate;
  bool isShow = false;
  DateTime selectTime;
  Map checkAddressObj;
  User user;
  bool isAddressCheck = false;
  bool isNew = false;

  OrderAddressScreenState({this.products, this.restaurant}) {
    if (Restaurant.isRestaurantClosed(restaurant)) {
      calculateNextAvailableDate(restaurant);
      selectTime = nextAvailableDate;
      userOrder.orderClientInfo = new OrderClientInfo();
      userOrder.orderClientInfo.date =
          new DateFormat("yyyy/MM/dd+HH:mm:ss").format(nextAvailableDate);
    }
  }

  @override
  initState() {
    super.initState();
    initVars();
  }

  void initVars() async {
    final prefs = await SharedPreferences.getInstance();
    user = User.fromJson(json.decode(prefs.get('user')));
    //values.clear();
    for (var a in user.address) {
      Address address = new Address();
      address.address = a['address'];
      address.addressId = a['id_direccion'];
      address.cityId = a['id_city'];
      address.lat = a['lat'];
      address.lng = a['lng'];
      address.info = a['info'];
      values.add(address);
    }
    setState(() {});
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void checkAddress() async {
    showProgressDialog();
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    String url = ApiURL.apiURL;
    checkAddressObj = new Map();
    checkAddressObj['lat'] = address.lat;
    checkAddressObj['lng'] = address.lng;
    checkAddressObj['token'] = user.token;
    List ids = new List();
    ids.add(restaurant.id);
    checkAddressObj['negocios'] = ids;
    var body =
        checkAddressObj; //{'lat': lat, 'lng': lng, "type": type, 'token': token};
    Map<String, String> map = {
      'datos': jsonEncode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    final response =await http
        .post(url + ApiURL.urlCheckAddress.replaceAll('?', apiVersion),
            headers: headers, body: map)
        .timeout(Global.timeout);
    print(response.body);
    if (response.body != "") {
      handleCheckAddressResponse(response.body);
    } else {
      handleCheckAddressResponse("");
    }
  }

  void handleCheckAddressResponse(var responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      print('algo');
      if (map['resultado']['code'] == 0) {
        isAddressCheck = true;
        for (var value in map['resultado']['negocios']) {
          print(value);
          restaurant.minOrder = value['minorder'].toString();
          restaurant.transport = value['transport'].toString();
          restaurant.free = value['free'].toString();
        }
        if (isNew) {
          openPaymentScreen();
        }
      } else if (map['resultado']['code'] == -3) {
        showInSnackBar('Sesion iniciada en otro dispositivo');
      } else {
        showInSnackBar('El negocio no reparte en esta dirección');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      backgroundColor: Color(0xfff8f9fa),
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Entrega de pedido',
          style: new TextStyle(
            color: secondaryColor,
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          child: Container(
            //height: screenSize.height * 1.1,
            child: Column(
              children: <Widget>[
                Restaurant.isRestaurantClosed(restaurant)
                    ? Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.access_time),
                                Text(
                                  'Horario de entrega',
                                  style: GoogleFonts.varelaRound(
                                      textStyle:
                                          TextStyle(fontWeight: FontWeight.bold)),
                                )
                              ],
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.all(8),
                              child: Text(
                                  'El pedido realizado incluye comercio que '
                                  'estan cerrados en estos momentos',
                                  style: GoogleFonts.varelaRound(
                                      textStyle: TextStyle()))),
                          Container(
                              margin: EdgeInsets.all(8),
                              height: screenSize.height / 15,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      restaurant.name,
                                      style: GoogleFonts.varelaRound(
                                          textStyle: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      ' Cerrado',
                                      style: GoogleFonts.varelaRound(),
                                    ),
                                  )
                                ],
                              )),
                          Container(
                            child: Text(
                                'Cuanto quieres que se entregue el pedido?',
                                style: GoogleFonts.varelaRound()),
                            margin: EdgeInsets.all(8),
                          ),
                          Card(
                            margin: EdgeInsets.all(8),
                            child: Column(
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                    margin: EdgeInsets.all(8),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          'Lo antes posible ' +
                                              new DateFormat("dd MMM. HH:mm")
                                                  .format(nextAvailableDate),
                                          style: GoogleFonts.varelaRound(
                                              textStyle: TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        isEdit ? null : Icon((Icons.check)),
                                      ].where((child) => child != null).toList(),
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      isEdit = false;
                                    });
                                  },
                                ),
                                new Divider(
                                  color: Colors.grey,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isEdit = true;
                                    });
                                  },
                                  child: Container(
                                      margin: EdgeInsets.all(8),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('Un dia y hora concreta',
                                              style: GoogleFonts.varelaRound(
                                                  textStyle: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold))),
                                          isEdit ? Icon((Icons.check)) : null
                                        ]
                                            .where((child) => child != null)
                                            .toList(),
                                      )),
                                ),
                              ],
                            ),
                          ),
                          isEdit
                              ? Container(
                                  height: screenSize.height / 11,
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Icon(Icons.calendar_today),
                                              Text('Fecha:')
                                            ],
                                          ),
                                          new Expanded(
                                            flex: 4,
                                            child: new InputDropdown(
                                              valueText: selectedDate != null
                                                  ? new DateFormat("dd MMM yyy")
                                                      .format(selectedDate)
                                                  : 'Selecciona una fecha',
                                              onPressed: () {
                                                selectDate(context);
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      selectedDate != null
                                          ? Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Icon(Icons.access_time),
                                                    Text('Hora:')
                                                  ],
                                                ),
                                                new Expanded(
                                                  flex: 3,
                                                  child: new InputDropdown(
                                                    valueText:
                                                        new DateFormat("HH:mm")
                                                            .format(selectTime),
                                                    onPressed: () {
                                                      selectSchedule(
                                                          selectedDate);
                                                    },
                                                  ),
                                                )
                                              ],
                                            )
                                          : null,
                                    ].where((child) => child != null).toList(),
                                  ),
                                )
                              : null,
                        ].where((child) => child != null).toList(),
                      )
                    : Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.directions_car),
                            Text(
                              'Direccion de entrega',
                              style: GoogleFonts.varelaRound(
                                  textStyle:
                                      TextStyle(fontWeight: FontWeight.bold)),
                            )
                          ],
                        ),
                        margin: EdgeInsets.all(16),
                      ),
                Container(
                    margin: new EdgeInsets.all(16),
                    width: screenSize.width / 1.1,
                    height: screenSize.height / 14,
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(style: BorderStyle.solid, width: 0.80),
                    ),
                    child: new DropdownButtonHideUnderline(
                      child: DropdownButton<Address>(
                        isExpanded: true,
                        hint: Text('Selecciona una direccion'),
                        items: values
                            .map<DropdownMenuItem<Address>>((Address value) {
                          return DropdownMenuItem<Address>(
                            value: value,
                            child: Text(value.address),
                          );
                        }).toList(),
                        onChanged: (Address value) {
                          setState(() {
                            address = value;
                          });
                          checkAddress();
                        },
                        value: address,
                        isDense: true,
                      ),
                    )),
                Padding(
                  padding: EdgeInsets.all(4),
                ),
                RoundedButton(
                  buttonName: 'Agregar direccion de entrega',
                  onTap: () {
                    openAddressScreen();
                  },
                  width: screenSize.width / 1.1,
                  height: screenSize.height / 13,
                  bottomMargin: 10.0,
                  borderWidth: 1.0,
                  buttonColor: terciaryColor,
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                RoundedButton(
                  buttonName: 'Continuar',
                  onTap: () {
                    if (isAddressCheck) {
                      openPaymentScreen();
                    } else {
                      showInSnackBar('La direccion debe ser validad primero');
                    }
                  },
                  width: screenSize.width / 1.1,
                  height: screenSize.height / 13,
                  bottomMargin: 10.0,
                  borderWidth: 1.0,
                  buttonColor: secondaryColor,
                ),
              ].where((child) => child != null).toList(),
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  void openAddressScreen() async {
    var response = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new AddressScreen(
                  isOder: true,
                )));
    if (response == null) {
      address = null;
      values.clear();
      initVars();
    } else {
      values.clear();
      initVars();
      final prefs = await SharedPreferences.getInstance();
      user = User.fromJson(json.decode(prefs.get('user')));
      print('segundo token:  ' + user.token);
      values.add(response);
      setState(() {});
      address = response;
      isNew = true;
      checkAddress();
    }
  }

  void openPaymentScreen() async {
    if (address != null) {
      Order order = new Order();
      order.products = new List();
      for (var p in products) {
        order.products.add(p);
      }
      order.businessId = restaurant.id;
      order.transport = restaurant.transport;
      order.transportFloat = 2;
      order.minOrder = restaurant.minOrder;
      order.free = restaurant.free;
      order.open = restaurant.open;
      order.schedules = restaurant.schedules;
      order.name = restaurant.name;
      order.totalRestaurant = 19;
      userOrder.order = new List();
      userOrder.order.add(order);
      if (userOrder.orderClientInfo == null) {
        userOrder.orderClientInfo = new OrderClientInfo();
      }
      if (isEdit) {
        nextAvailableDate = new DateTime(selectedDate.year, selectedDate.month,
            selectedDate.day, selectTime.hour, selectTime.minute);
        userOrder.orderClientInfo.date =
            new DateFormat("yyyy/MM/dd+HH:mm:ss").format(nextAvailableDate);
      }
      userOrder.orderClientInfo.cityId = address.cityId;
      userOrder.orderClientInfo.addressId = address.addressId;
      userOrder.orderClientInfo.paymentWayId =
          3; //// 1: Paypal  - 3: Contrareembolso
      userOrder.orderClientInfo.paypalId = '';
      userOrder.orderClientInfo.discountClientId = '';
      userOrder.orderClientInfo.comment = '';
      userOrder.orderClientInfo.discountGlobalId = '';
      await Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (context) => new PaymentScreen(
                    restaurant: restaurant,
                    products: products,
                    userOrder: userOrder,
                  )));
    } else {
      showInSnackBar('Debe seleccionar una direccion');
    }
  }

  void calculateNextAvailableDate(Restaurant res) {
    try {
      var todayDayNumber = new DateTime.now().weekday - 1;
      var weekDays = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];
      var now = new DateTime.now();
      var todayDayLetter = weekDays[todayDayNumber];
      var found = false;
      var iterateDayNumber = todayDayNumber;
      var numberOfDaysAhead = 0;
      var schedules = res.schedules;
      for (var p in schedules[todayDayLetter]) {
        print(p['hora_inicio']);
        var restaurantDateTime = timeStrToDate(p['hora_inicio']);
        print(restaurantDateTime);
        if (now.isBefore(restaurantDateTime)) {
          found = true;
          nextAvailableDate = restaurantDateTime;
          break;
        }
      }
      while (!found) {
        if (iterateDayNumber == 6) {
          iterateDayNumber = 0;
        } else {
          iterateDayNumber += 1;
        }
        numberOfDaysAhead += 1;
        var iterateDayLetter = weekDays[iterateDayNumber];
        if (schedules[iterateDayLetter].length > 0) {
          var iterateDate =
          timeStrToDate(schedules[iterateDayLetter][0]['hora_inicio']);
          iterateDate = new DateTime(
              iterateDate.year,
              iterateDate.month,
              iterateDate.day + numberOfDaysAhead,
              iterateDate.hour,
              iterateDate.minute);
          nextAvailableDate = iterateDate;
          found = true;
        }
      }
    } catch (e) {
      e.toString();
    }
  }

  DateTime timeStrToDate(String timeStr) {
    String date =
        new DateFormat("yyyy-MM-dd").format(new DateTime.now()) + ' ' + timeStr;
    return DateTime.parse(date);
  }

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime(nextAvailableDate.year, nextAvailableDate.month,
            nextAvailableDate.day),
        firstDate: DateTime(nextAvailableDate.year, nextAvailableDate.month,
            nextAvailableDate.day),
        lastDate: DateTime(nextAvailableDate.year, nextAvailableDate.month,
            nextAvailableDate.day + 6));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        selectTime = getOpenTime(selectedDate, 0);
      });
  }

  DateTime getOpenTime(DateTime dateTime, int schedules) {
    var todayDayNumber = dateTime.weekday;
    var weekDays = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];
    var todayDayLetter = weekDays[todayDayNumber];
    return timeStrToDate(
        restaurant.schedules[todayDayLetter][schedules]['hora_inicio']);
  }

  DateTime getCloseTime(DateTime dateTime, int schedules) {
    var todayDayNumber = dateTime.weekday;
    var weekDays = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];
    var todayDayLetter = weekDays[todayDayNumber];
    return timeStrToDate(
        restaurant.schedules[todayDayLetter][schedules]['hora_fin']);
  }

  void showTimePicker(String minDateTime, String maxDateTime) {
    DatePicker.showDatePicker(
      context,
      minDateTime: DateTime.parse(minDateTime),
      maxDateTime: DateTime.parse(maxDateTime),
      initialDateTime: DateTime.parse(minDateTime),
      dateFormat: 'HH:mm',
      pickerMode: DateTimePickerMode.time,
      pickerTheme: DateTimePickerTheme(
        title: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(color: Color(0xFFEFEFEF)),
              width: double.infinity,
              height: 56.0,
              alignment: Alignment.center,
              child: GestureDetector(
                child: Text('Seleccione hora'),
              ),
            ),
          ],
        ),
        titleHeight: 56.0,
      ),
      onCancel: () {
        debugPrint('onCancel');
      },
      onChange: (dateTime, List<int> index) {
        setState(() {
          selectTime = dateTime;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          selectTime = dateTime;
        });
      },
    );
  }

  void selectSchedule(DateTime selectedDate) async {
    String minDateTime = '2021-11-25 12:45:10';
    String maxDateTime = '2021-11-25 22:45:10';
    var todayDayNumber = selectedDate.weekday;
    var weekDays = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];
    var todayDayLetter = weekDays[todayDayNumber];

    print(restaurant.schedules[todayDayLetter].length);
    if (restaurant.schedules[todayDayLetter].length == 1) {
      minDateTime = new DateFormat("yyyy-MM-dd HH:mm:ss")
          .format(getOpenTime(selectedDate, 0));
      maxDateTime = new DateFormat("yyyy-MM-dd HH:mm:ss")
          .format(getCloseTime(selectedDate, 0));
      showTimePicker(minDateTime, maxDateTime);
    } else if (restaurant.schedules[todayDayLetter].length == 2) {
      if (await MyDialog.showNativePopUpWith2Buttons(
          context,
          'Ya sabes que pedir?',
          'Escribenos por whatsapp al  ',
          'Comida',
          'Cena')) {
        minDateTime = new DateFormat("yyyy-MM-dd HH:mm:ss")
            .format(getOpenTime(selectedDate, 1));
        maxDateTime = new DateFormat("yyyy-MM-dd HH:mm:ss")
            .format(getCloseTime(selectedDate, 1));
        showTimePicker(minDateTime, maxDateTime);
      } else {
        minDateTime = new DateFormat("yyyy-MM-dd HH:mm:ss")
            .format(getOpenTime(selectedDate, 0));
        maxDateTime = new DateFormat("yyyy-MM-dd HH:mm:ss")
            .format(getCloseTime(selectedDate, 0));
        showTimePicker(minDateTime, maxDateTime);
      }
    }
  }
}
