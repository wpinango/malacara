import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/country.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../api_url.dart';
import '../../global.dart';
import '../../theme.dart';

class CountrySelectedScreen extends StatefulWidget {
  const CountrySelectedScreen({Key key}) : super(key: key);

  @override
  CountrySelectedScreenState createState() => new CountrySelectedScreenState();
}

class CountrySelectedScreenState extends State<CountrySelectedScreen> {
  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  Country selectedCountry = new Country();
  String country;
  List<String> values = ['España', 'Ecuador', 'Panama'];
  List<Country> countryList = new List();
  var timeout = const Duration(seconds: 20);

  @override
  void initState() {
    super.initState();
    getSelectedCounty();
    requestVersion();
  }

  void getSelectedCounty() async {
    final prefs = await SharedPreferences.getInstance();
    selectedCountry =
        Country.fromJson(json.decode(prefs.get('selectedCountry')));
    setState(() {
      country = selectedCountry.name;
    });
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void requestVersion() async {
    showProgressDialog();
    try {
      var body = {'version': 'v020300', 'platform': 'android', 'token': -1};
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post(ApiURL.apiURL + ApiURL.getVersion, headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleVersionResponse(response.body);
      } else {
        handleVersionResponse("");
      }
    } catch (e) {
      handleVersionResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleVersionResponse(final responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      String apiVersion = map['resultado']['vapi'];
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('apiVersion', apiVersion);
      for (var item in map['resultado']['businesses']) {
        Country country = new Country();
        country.name = item[0];
        country.url = item[1];
        country.code = item[2].toString().split('-')[1];
        country.completeCode = item[2];
        country.protocol = item[3];
        countryList.add(country);
        if (!values.contains(country.name)) {
          values.add(country.name);
        }
      }
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(
            Global.appName,
            style: new TextStyle(
              fontSize: Theme.of(context).platform == TargetPlatform.iOS
                  ? 17.0
                  : 20.0,
            ),
          ),
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false)),
          elevation:
              Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
        ),
        key: scaffoldKey,
        body: ModalProgressHUD(
          child: SingleChildScrollView(
            controller: scrollController,
            child: new Container(
                height: screenSize.height / 2,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: new Text(
                        'Elige el país donde quieres hacer los pedidos',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.black,
                        ),
                      ),
                      margin: EdgeInsets.all(8),
                    ),
                    Container(
                        margin: new EdgeInsets.only(
                            bottom: 16.0, top: 16.0, right: 8, left: 8),
                        width: screenSize.width,
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(
                              color: primaryColor,
                              style: BorderStyle.solid,
                              width: 0.80),
                        ),
                        child: new DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                              hint: Text('Seleccione un pais'),
                              items: values.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (String value) {
                                setState(() {
                                  country = value;
                                });
                              },
                              isDense: true,
                              value: country),
                        )),
                    Container(
                      margin: EdgeInsets.only(right: 8, left: 8),
                      child: new RoundedButton(
                        buttonName: "Elegir",
                        onTap: onButtonPressed,
                        width: screenSize.width / 3,
                        height: 50.0,
                        bottomMargin: 10.0,
                        borderWidth: 0.0,
                        buttonColor: primaryColor,
                      ),
                    ),
                  ],
                )),
          ),
          inAsyncCall: isInAsyncCall,
          opacity: 0.5,
          progressIndicator: NativeLoadingIndicator(),
        ));
  }

  onButtonPressed() async {
    if (country != null) {
      for (var c in countryList) {
        if (c.name == country) {
          setState(() {
            selectedCountry = c;
          });
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('selectedCountry', json.encode(selectedCountry));
          prefs.setString(Global.keyApiUrl,
              selectedCountry.protocol + '://' + selectedCountry.url);
          showConfirmDialog();
        }
      }
    } else {
      showInSnackBar("Debe seleccionar un pais");
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showConfirmDialog() async {
    if (await MyDialog.showNativePopUpWith2Buttons(context, 'Informacion',
        'Se selecciono el pais correctamente', 'Cancelar', 'Aceptar')) {
      Navigator.pop(context, true);
    }
  }
}
