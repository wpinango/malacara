import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/models/business.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/restaurant.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/models/user_location.dart';
import 'package:malacara/screens/restaurant/restaurant_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../../api_url.dart';
import '../../global.dart';
import '../../theme.dart';

class BusinessScreen extends StatefulWidget {
  final List<Business> business;

  const BusinessScreen({Key key, this.business}) : super(key: key);

  @override
  BusinessScreenState createState() =>
      new BusinessScreenState(business: this.business);
}

class BusinessScreenState extends State<BusinessScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  List<ListItem> items;
  List<Business> business;
  var timeout = const Duration(seconds: 20);
  Country selectedCountry;
  var f;

  @override
  initState() {
    super.initState();
    getSelectedCounty();
  }

  void getSelectedCounty() async {
    final prefs = await SharedPreferences.getInstance();
    selectedCountry =
        Country.fromJson(json.decode(prefs.get('selectedCountry')));
    f = Global.getCurrencySymbol(selectedCountry);
    setState(() {});
  }

  BusinessScreenState({this.business}) {
    items = new List();
    if (business != null) {
      items.add(HeadingItem('Negocios abiertos'));
      for (var b in business) {
        if (b.open == '1') {
          items.add(MessageItem(b));
        }
      }
      items.add(HeadingItem('Negocios cerrados'));
      for (var b in business) {
        if (b.open == '0') {
          items.add(MessageItem(b));
        }
      }
      //print('valores ' + business[0].name);
    } else {
      items.add(HeadingItem('Negocios abiertos'));
      items.add(HeadingItem('Negocios cerrados'));
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  String getClosedRestaurants() {
    int count = 0;
    if (business != null) {
      for (var b in business) {
        if (b.open == '0') {
          count++;
        }
      }
    }
    return count.toString();
  }

  String getOpenedRestaurants() {
    int count = 0;
    if (business != null) {
      for (var b in business) {
        if (b.open == '1') {
          count++;
        }
      }
    }
    return count.toString();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: onWillPop1,
      child: new Scaffold(
        key: scaffoldKey,
        appBar: new AppBar(
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, true)),
          title: new Text(
            Global.appName,
            style: new TextStyle(
              color: secondaryColor,
              fontSize: Theme.of(context).platform == TargetPlatform.iOS
                  ? 17.0
                  : 20.0,
            ),
          ),
          actions: <Widget>[
            /*IconButton(
              icon: Icon(
                Icons.search,
                color: white,
              ),
              onPressed: null,
            ),
            IconButton(
              icon: Icon(
                Icons.fastfood,
                color: white,
              ),
              onPressed: null,
            ),*/
          ],
          backgroundColor: primaryColor,
          iconTheme: IconThemeData(color: secondaryColor),
          elevation:
              Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
        ),
        body: ModalProgressHUD(
          child: ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
              final item = items[index];
              if (item is HeadingItem) {
                return ListTile(
                    title: Container(
                        color: item.heading == 'Negocios abiertos'
                            ? secondaryColor
                            : Colors.black,
                        height: screenSize.height / 14,
                        width: screenSize.width,
                        padding: EdgeInsets.all(8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              item.heading,
                              textAlign: TextAlign.start,
                              style: GoogleFonts.varelaRound(
                                  textStyle: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                      color: Colors.white)),
                            ),
                            new Text(
                                item.heading == 'Negocios abiertos'
                                    ? getOpenedRestaurants()
                                    : getClosedRestaurants(),
                                style: GoogleFonts.varelaRound(
                                  textStyle: TextStyle(
                                      color: white,
                                      fontWeight: FontWeight.bold),
                                )),
                            /*new CircleAvatar(
                              //backgroundColor: white,
                              maxRadius: 12,
                              child:
                            ),*/
                          ],
                        )));
              } else if (item is MessageItem) {
                return Container(
                  margin: EdgeInsets.only(right: 12, left: 12),
                  child: Card(
                      child: ListTile(
                    leading: Container(
                      decoration: BoxDecoration(boxShadow: [
                        BoxShadow(color: Colors.grey, spreadRadius: 1),
                      ]),
                      child: new Image.network(
                        item.business.logo,
                        fit: BoxFit.cover,
                        height: screenSize.height / 2,
                        width: screenSize.width / 8,
                        alignment: Alignment.center,
                      ),
                    ),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(top: 4.0)),
                        Text(
                          item.business.name,
                          style: GoogleFonts.varelaRound(
                              textStyle: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold)),
                        ),
                        Padding(padding: EdgeInsets.only(top: 4.0)),
                        Text(
                          item.business.kitchenType.length > 0
                              ? item.business.kitchenType[0]['name']
                              : '-',
                          style: GoogleFonts.varelaRound(
                              textStyle: TextStyle(
                            fontSize: 12,
                          )),
                        )
                      ],
                    ),
                    subtitle: Column(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(top: 4.0)),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              color: Colors.grey,
                              size: 16,
                            ),
                            Flexible(
                                child: Text(item.business.address,
                                    style: GoogleFonts.varelaRound(
                                      textStyle: TextStyle(
                                          fontSize: 10, color: Colors.grey),
                                    ))),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 4.0)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                                child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.access_time,
                                  color: Colors.grey,
                                  size: 14,
                                ),
                                Text(
                                  item.business.tmedio + 'mins',
                                  style: GoogleFonts.varelaRound(
                                      textStyle: TextStyle(
                                          fontSize: 12, color: Colors.grey)),
                                )
                              ],
                            )),
                            Padding(
                              padding: EdgeInsets.only(left: 15, right: 15),
                            ),
                            Flexible(
                                child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.shopping_cart,
                                  color: Colors.grey,
                                  size: 14,
                                ),
                                Text(
                                  'Min:' +
                                      f.format(double.parse(
                                          item.business.minOrder != 0
                                              ? item.business.minOrder
                                              : '0')),
                                  style: GoogleFonts.varelaRound(
                                      textStyle: TextStyle(
                                          fontSize: 12, color: Colors.grey)),
                                ),
                              ],
                            )),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 4.0)),
                      ],
                    ),
                    onTap: () {
                      //print(item.business.name);
                      requestBusinessData(item.business);
                    },
                  )),
                );
              } else {
                return null;
              }
            },
          ),
          inAsyncCall: isInAsyncCall,
          opacity: 0.5,
          progressIndicator: NativeLoadingIndicator(),
        ),
      ),
    );
  }

  void requestBusinessData(Business b) async {
    showProgressDialog();
    try {
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      String url = ApiURL.apiURL; //prefs.get(Global.keyApiUrl);
      String token;
      UserLocation userLocation =
          UserLocation.fromJson(json.decode(prefs.getString('location')));
      //print(userLocation.longitude);
      if (prefs.getBool('login') != null) {
        if (prefs.getBool('login')) {
          User user = User.fromJson(json.decode(prefs.get('user')));
          token = user.token;
        } else {
          token = '-1';
        }
      } else {
        token = '-1';
      }
      var body = {
        'lat': userLocation.latitude,
        'lng': userLocation.longitude,
        "negocio": b.id,
        'token': token
      };
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post(url + ApiURL.urlGetBusinessData.replaceAll('?', apiVersion ?? "v050100"),
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleBusinessData(response.body);
      } else {
        handleBusinessData("");
      }
    } catch (e) {
      handleBusinessData("");
      print('algo salio mal ' + e.toString());
      showInSnackBar('Algo salio mal');
    }
  }

  void handleBusinessData(String responseBody) async {
    cancelProgressDialog();
    try {
      if (responseBody != '') {
        Map<String, dynamic> map = json.decode(responseBody);
        if (map['resultado']['code'] == 0) {
          Restaurant restaurant = Restaurant.fromJson(
              json.decode(json.encode(map['resultado']['negocio'])));
          List<MenuCard> menuCard = (map['resultado']['carta'] as List)
              .map((p) => MenuCard.fromJson(p))
              .toList();
          await Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) =>
                  new RestaurantScreen(
                    restaurant: restaurant,
                    menuCards: menuCard,
                  )));
        } else {
          showInSnackBar('Algo salio mal');
        }
      } else {
        showInSnackBar('Algo salio mal');
      }
    } catch (e) {
      e.toString();
    }
  }

  Future<bool> onWillPop1() async {
    Navigator.pop(context, true);
  }
}

abstract class ListItem {}

class HeadingItem implements ListItem {
  final String heading;

  HeadingItem(this.heading);
}

class MessageItem implements ListItem {
  Business business;

  MessageItem(this.business);
}
