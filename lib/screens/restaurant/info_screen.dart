import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/restaurant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../global.dart';
import '../../theme.dart';

class InfoScreen extends StatefulWidget {
  final Restaurant restaurant;

  const InfoScreen({Key key, this.restaurant}) : super(key: key);

  @override
  InfoScreenState createState() => new InfoScreenState(restaurant: restaurant);
}

class InfoScreenState extends State<InfoScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  Restaurant restaurant;
  Country selectedCountry;
  var f;

  InfoScreenState({this.restaurant});

  @override
  initState() {
    super.initState();
    initVars();
  }

  void initVars() async {
    final prefs = await SharedPreferences.getInstance();
    selectedCountry =
        Country.fromJson(json.decode(prefs.get('selectedCountry')));
    setState(() {
      f = Global.getCurrencySymbol(selectedCountry);
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Informacion',
          style: new TextStyle(
            color: Colors.white,
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          controller: scrollController,
          child: new Container(
            //height: screenSize.height * 1.6,
            padding: new EdgeInsets.only(top: 8, right: 8, left: 8, bottom: 8),
            child: new Column(
              children: <Widget>[
                Card(
                    child: ListTile(
                      leading: new Image.network(
                        restaurant.logo,
                        fit: BoxFit.cover,
                        height: screenSize.height / 5,
                        width: screenSize.width / 3.5,
                        alignment: Alignment.center,
                      ),
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(top: 4.0)),
                          Text(
                            restaurant.name,
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          Padding(padding: EdgeInsets.only(top: 4.0)),
                          Text(
                            restaurant.foodType,
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          )
                        ],
                      ),
                      subtitle: Column(
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(top: 4.0)),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.location_on,
                                color: Colors.grey,
                                size: 10,
                              ),
                              Flexible(
                                  child: Text(
                                    restaurant.address,
                                    style: TextStyle(fontSize: 10, color: Colors.grey),
                                  )),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 4.0)),
                        ],
                      ),
                    )),
                Padding(padding: EdgeInsets.only(top: 8.0)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text('Horario de apertura'),
                    Restaurant.isRestaurantClosed(restaurant)
                        ? Container(
                      padding: EdgeInsets.all(8),
                      color: Colors.red,
                      child: Text(
                        'Cerrado',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                        : Container(
                      padding: EdgeInsets.all(8),
                      color: Colors.green,
                      child: Text(
                        'Abierto',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 8.0)),
                restaurant.schedules != null ? getSchedule() : null,
                /*new Container(
                  height: screenSize.height / 2,
                  child: new ListView.builder(
                    itemCount: restaurant.schedules.length,
                    itemBuilder: (BuildContext context, int index) {
                      var key = restaurant.schedules.keys.toList();
                      var values = restaurant.schedules.values.toList();
                      return new ListTile(
                        leading: Text(getFullNameDay(key[index])),
                        trailing: Column(
                          children: getSchedules(values[index]),
                        ),
                      );
                    },
                  ),
                ),*/
                Padding(padding: EdgeInsets.only(top: 8.0)),
                restaurant.photos.length > 0
                    ? SizedBox(
                  height: screenSize.height / 4,
                  width: screenSize.width,
                  child: Carousel(images: getPhotos()),
                )
                    : null,
                Padding(padding: EdgeInsets.only(top: 8.0)),
                Container(
                  margin: EdgeInsets.only(top: 8, left: 8),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Sobre nosotros',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 8.0)),
                restaurant.description != null
                    ? Container(
                  margin: EdgeInsets.only(top: 8, left: 8),
                  alignment: Alignment.centerLeft,
                  child: Html(data: restaurant.description),
                )
                    : null,
              ].where((child) => child != null).toList(),
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  List<dynamic> getPhotos() {
    List<dynamic> ph = new List();
    for (var p in restaurant.photos) {
      ph.add(NetworkImage(p[1]));
    }
    return ph;
  }

  String getFullNameDay(String letter) {
    if (letter == 'L') {
      return 'Lunes';
    } else if (letter == 'M') {
      return 'Martes';
    } else if (letter == 'X') {
      return 'Miercoles';
    } else if (letter == 'J') {
      return 'Jueves';
    } else if (letter == 'V') {
      return 'Viernes';
    } else if (letter == 'S') {
      return 'Sabado';
    } else if (letter == 'D') {
      return 'Domingo';
    }
  }

  List<Widget> getSchedules(var items) {
    List<Widget> h = new List();
    for (var item in items) {
      h.add(Text('De ' +
          trimSeconds(item['hora_inicio']) +
          ' a ' +
          trimSeconds(item['hora_fin'])));
    }
    return h;
  }

  String trimSeconds(String hour) {
    return hour.split(':')[0] + ':' + hour.split(':')[1];
  }

  Widget getSchedule() {
    List<Widget> s = new List();
    restaurant.schedules.forEach((key, value) {
      s.add(ListTile(
        leading: Text(getFullNameDay(key)),
        trailing: Column(
          children: getSchedules(value),
        ),
      ));
    });
    return Column(
      children: s,
    );
  }
}