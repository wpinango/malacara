import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:malacara/bloc/shopping_car_bloc.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/components/own_expantiontitle.dart';
import 'package:malacara/models/choise.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/restaurant.dart';
import 'package:malacara/screens/order/order_summary_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:html/parser.dart';

import '../../global.dart';
import '../../routes.dart';
import '../../theme.dart';
import 'customize_order_screen.dart';
import 'info_screen.dart';

class RestaurantScreen extends StatefulWidget {
  final Restaurant restaurant;
  final List<MenuCard> menuCards;

  const RestaurantScreen({Key key, this.restaurant, this.menuCards})
      : super(key: key);

  @override
  RestaurantScreenState createState() => new RestaurantScreenState(
      restaurant: this.restaurant, menuCards: this.menuCards);
}

class RestaurantScreenState extends State<RestaurantScreen> {
  Restaurant restaurant;
  List<MenuCard> menuCards;
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  Country selectedCountry;
  var f;
  double amount = 0;
  ShoppingCartBloc shoppingCartBloc = new ShoppingCartBloc();

  RestaurantScreenState({this.restaurant, this.menuCards});

  @override
  initState() {
    super.initState();
    getSelectedCounty();
  }

  void getSelectedCounty() async {
    final prefs = await SharedPreferences.getInstance();
    selectedCountry =
        Country.fromJson(json.decode(prefs.get('selectedCountry')));
    f = Global.getCurrencySymbol(selectedCountry);
    setState(() {});
  }

  String getSubItemsCount(MenuCard menuCard) {
    int count = 0;
    for (var m in menuCard.products) {
      count++;
    }
    return count.toString();
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  String parseHtmlString(String htmlString) {
    var document = parse(htmlString);
    String parsedString = parse(document.body.text).documentElement.text;
    return parsedString;
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      backgroundColor: Color(0xfff8f9fa),
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          Global.appName,
          style: new TextStyle(
            color: secondaryColor,
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.info,
              color: white,
            ),
            onPressed: () => {openInfoScreen()},
          ),
        ],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              new Container(
                  constraints: new BoxConstraints.expand(
                    height: screenSize.height / 4,
                  ),
                  alignment: Alignment.bottomLeft,
                  padding: new EdgeInsets.only(left: 16.0, bottom: 8.0),
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new NetworkImage(restaurant.banner),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Center(
                        child: Text(
                          restaurant.name,
                          style: GoogleFonts.varelaRound(
                              textStyle: TextStyle(
                                color: white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              )),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 4.0)),
                      Text(restaurant.foodType,
                          style: GoogleFonts.varelaRound(
                            textStyle: TextStyle(
                              color: white,
                              fontSize: 14,
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(top: 8.0)),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.access_time,
                                  color: Colors.white,
                                  size: 12,
                                ),
                                Text(
                                  restaurant.tmedio + 'mins',
                                  style: GoogleFonts.varelaRound(
                                      textStyle: TextStyle(
                                          fontSize: 12, color: Colors.white)),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.shopping_cart,
                                  color: Colors.white,
                                  size: 12,
                                ),
                                Text(
                                  'Min:' +
                                      f.format(int.parse(
                                          restaurant.minOrder.toString())),
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.white),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  )),
              new Container(
                height:
                Product.getTotalAmount(shoppingCartBloc.cart.products) > 0.0
                    ? screenSize.height / 1.9
                    : screenSize.height / 1.6,
                child: new ListView.builder(
                  itemCount: menuCards.length,
                  itemBuilder: (BuildContext context, int index) {
                    return new OwnExpansionTile(
                      headerBackgroundColor: secondaryColor,
                      iconColor: white,
                      title: Row(
                        children: <Widget>[
                          Text(
                            getSubItemsCount(menuCards[index]),
                            style: GoogleFonts.varelaRound(
                                textStyle: TextStyle(
                                    color: white, fontWeight: FontWeight.bold)),
                          ),
                          Padding(
                              padding: EdgeInsets.only(left: 8.0, right: 8)),
                          Container(
                            width: screenSize.width / 1.6,
                            child: Text(
                              menuCards[index].name,
                              style: GoogleFonts.varelaRound(
                                  textStyle: TextStyle(color: white)),
                            ),
                          ),
                        ],
                      ),
                      children: menuCards[index]
                          .products
                          .map((data) => getItemsWidget(screenSize, data))
                          .toList(),
                    );
                  },
                ),
              ),
              //getButtonConfirm(screenSize),
            ].where((child) => child != null).toList(),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      bottomNavigationBar: getButtonConfirm(screenSize),
    );
  }

  Widget getItemsWidget(Size screenSize, var data) {
    List<Widget> items = new List();
    items.add(Container(
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Color(0xffe2e2e2),
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        children: [
          Image(
            image: NetworkImage(data['imagen']),
            width: screenSize.width / 4.8,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(2),
                padding: EdgeInsets.only(left: 8),
                width: screenSize.width / 1.6,
                child: Text(
                  data['nombre'],
                  style: GoogleFonts.varelaRound(
                      textStyle:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                ),
              ),
              Container(
                margin: EdgeInsets.all(2),
                padding: EdgeInsets.only(left: 8),
                width: screenSize.width / 1.6,
                child: Text(
                  parseHtmlString(data['descripcion']),
                  style: TextStyle(fontSize: 12),
                ),
              ),
              customProduct(data),
              Padding(
                padding: EdgeInsets.all(4),
              ),
              Container(
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.only(left: 8),
                  child: Text(
                    f.format(double.parse(data['precio'])),
                    style: GoogleFonts.varelaRound(
                        textStyle: TextStyle(
                            fontSize: 14,
                            color: terciaryColor,
                            fontWeight: FontWeight.bold)),
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.remove),
                        onPressed: () {
                          data['cantidad'] = (data['cantidad'] ?? 0) > 0
                              ? (data['cantidad'] ?? 0) - 1
                              : 0;
                          setState(() {});
                        },
                      ),
                      Text((data['cantidad'] ?? 0).toString()),
                      IconButton(
                        icon: Icon(
                          Icons.add,
                        ),
                        onPressed: () {
                          data['cantidad'] = (data['cantidad'] ?? 0) + 1;
                          setState(() {});
                        },
                      ),
                    ],
                  ),
                  RaisedButton.icon(
                    onPressed: () {
                      data['cantidad'] > 0
                          ? Product.checkItemsInCar(Product.prepareData(data),
                          shoppingCartBloc.cart.products)
                          : null;
                      //products = List.from(Product.addTempToProducts(tempProducts));
                      updateAmount();
                      setState(() {});
                    },
                    icon: Icon(
                      Icons.shopping_cart,
                      color: white,
                    ),
                    label: Text(
                      'Agregar',
                      style: GoogleFonts.varelaRound(
                          textStyle:
                          TextStyle(color: white, fontWeight: FontWeight.bold)),
                    ),
                    color: secondaryColor,
                  )
                ],
              ),
            ].where((child) => child != null).toList(),
          ),
        ],
      ),
    ));
    return Column(
      children: items,
    );
  }

  void onAddToCarButton(var p) {
    Product.checkItemsInCar(p, shoppingCartBloc.cart.products);
    updateAmount();
  }

  void updateAmount() {
    amount = 0.0;
    setState(() {
      for (int i = 0; i < shoppingCartBloc.cart.products.length; i++) {
        setState(() {
          for (var p in shoppingCartBloc.cart.products[i].extras) {
            amount += ((double.parse(p['precio'].toString())) *
                shoppingCartBloc.cart.products[i].quantity);
          }
          for (int j = 0;
          j < shoppingCartBloc.cart.products[i].combinations.length;
          j++) {
            amount += (shoppingCartBloc.cart.products[i].combinations[j].price *
                shoppingCartBloc.cart.products[i].quantity);
          }
          amount += double.parse(shoppingCartBloc.cart.products[i].amount) *
              shoppingCartBloc.cart.products[i].quantity;
        });
      }
    });
  }

  bool isCustomizable(var p) {
    bool isCustom = false;
    List extras = p['extras'];
    if (extras.isNotEmpty) {
      isCustom = true;
    }
    return isCustom;
  }

  Widget customProduct(var p) {
    if (isCustomizable(p)) {
      return GestureDetector(
        onTap: () {
          openCustomizeScreen(Product.prepareData(p), p);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              'Personalizar el plato',
              style: TextStyle(fontSize: 12),
            ),
            Icon(
              Icons.edit,
              size: 12,
            )
          ],
        ),
      );
    } else {
      return null;
    }
  }

  Widget getButtonConfirm(Size s) {
    if (Product.getTotalAmount(shoppingCartBloc.cart.products) > 0.0) {
      return new RoundedButton(
        buttonName: 'Realizar pedido ' + f.format(amount),
        onTap: checkMinOrder,
        width: s.width / 1.2,
        height: s.height / 13,
        bottomMargin: 10.0,
        borderWidth: 1.0,
        buttonColor: secondaryColor,
      );
    }
  }

  void checkMinOrder() {
    if (double.parse(restaurant.minOrder.toString()) >
        Product.getTotalAmount(shoppingCartBloc.cart.products)) {
      showInSnackBar('El pedido no supera el monto minimo');
    } else {
      openOrderSummaryScreen();
    }
  }

  void openOrderSummaryScreen() async {
    await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new OrderSummaryScreen(
              restaurant: restaurant,
              products: shoppingCartBloc.cart.products,
            )));
  }

  void openCustomizeScreen(Product product, dynamic data) async {
    if (await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new CustomizeOrderScreen(
              restaurant: restaurant,
              product: product,
              data: data,
              products: shoppingCartBloc.cart.products,
            )))) {
      updateAmount();
      data['cantidad'] = shoppingCartBloc.cart
          .products[shoppingCartBloc.cart.products.indexOf(product)].quantity;
      setState(() {});
    }
  }

  void openInfoScreen() async {
    await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new InfoScreen(
              restaurant: restaurant,
            )));
  }

  void handleSelectedItem(
      Choice choice, var p, dynamic data, int quantity) async {
    if (choice.id == 0) {
      if (isCustomizable(p)) {
        openCustomizeScreen(Product.prepareData(p), data);
      } else {
        onAddToCarButton(p);
      }
    } else if (choice.id == 1) {
      Product.removeItemCar(p, shoppingCartBloc.cart.products);
      updateAmount();
    }
  }
}

const List<Choice> choices = const <Choice>[
  const Choice(id: 0, title: 'Añadir', icon: Icons.add),
  const Choice(id: 1, title: 'Remover', icon: Icons.remove),
];
