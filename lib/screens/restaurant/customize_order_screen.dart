import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/restaurant.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../global.dart';
import '../../theme.dart';
import '../../util.dart';
import 'extras_screen.dart';
import 'ingredients_screen.dart';

class CustomizeOrderScreen extends StatefulWidget {
  final Restaurant restaurant;
  final Product product;
  final List products;
  final dynamic data;

  const CustomizeOrderScreen(
      {Key key, this.restaurant, this.product, this.products, this.data})
      : super(key: key);

  @override
  CustomizeOrderScreenState createState() => new CustomizeOrderScreenState(
      restaurant: restaurant, product: product, products: products, data: data);
}

class CustomizeOrderScreenState extends State<CustomizeOrderScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  Restaurant restaurant;
  Product product;
  Country selectedCountry;
  List products;
  var f;
  dynamic data;
  List combinations;
  List extras;
  List ingredients;
  double amount;

  CustomizeOrderScreenState(
      {this.restaurant, this.product, this.products, this.data}) {
    initVars();
  }

  void initVars() {
    extras = data['extras'] ?? [];
    ingredients = data['ingredientes'] ?? [];
    combinations = data['combinaciones'] ?? [];
    getProductQuantity();
    if (combinations.length > 0) {
      for (int i = 0; i < combinations.length; i++) {
        combinationChange(combinations[i], 0);
      }
    }
    updateAmount();
  }

  @override
  initState() {
    super.initState();
    getSelectedCounty();
  }

  void getSelectedCounty() async {
    final prefs = await SharedPreferences.getInstance();
    selectedCountry =
        Country.fromJson(json.decode(prefs.get('selectedCountry')));
    setState(() {
      f = Global.getCurrencySymbol(selectedCountry);
    });
  }

  String getProductQuantity() {
    return product.quantity.toString();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Personaliza el plato',
          style: new TextStyle(
            color: Colors.white,
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: <Widget>[
              Card(
                child: Container(
                    padding: EdgeInsets.all(4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 4, top: 4),
                              child: Text(
                                Util.parseHtmlString(product.name),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              width: screenSize.width / 1.5,
                              margin: EdgeInsets.all(4),
                              child: Text(
                                Util.parseHtmlString(product.description),
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: TextStyle(fontSize: 11),
                                maxLines: 3,
                              ),
                            ),
                            product.extras.length > 0
                                ? Row(
                              children: <Widget>[
                                Container(
                                  margin:
                                  EdgeInsets.only(left: 4, bottom: 4),
                                  child: Text(
                                    'Extras: ',
                                    style: TextStyle(
                                        fontSize: 11,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Container(

                                  width: screenSize.width / 1.8,
                                  margin: EdgeInsets.only(
                                      left: 0, bottom: 4),
                                  child: Text(
                                    Product.getExtras(product, f),
                                    style: TextStyle(fontSize: 11),
                                  ),
                                ),
                              ],
                            )
                                : null
                          ].where((child) => child != null).toList(),
                        ),
                        Container(
                          margin: EdgeInsets.all(4),
                          child: Text(f.format(double.parse(product.amount))),
                        ),
                      ],
                    )),
              ),
              Padding(padding: EdgeInsets.only(top: 4.0)),
              Card(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text('Cantidad'),
                            margin: EdgeInsets.all(4),
                          ),
                          Row(
                            children: <Widget>[
                              IconButton(
                                icon: Image.asset('assets/circle-more.png'),
                                onPressed: () => {
                                  removeProductQuantity(),
                                  setState(() {
                                    getProductQuantity();
                                  })
                                },
                              ),
                              Text(getProductQuantity()),
                              IconButton(
                                icon: Image.asset('assets/circle-minus.png'),
                                onPressed: () => {
                                  addProductQuantity(),
                                  setState(() {
                                    getProductQuantity();
                                  })
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                      new Divider(
                        color: Colors.grey,
                      ),
                      getExtrasSelection(),
                      new Divider(
                        color: Colors.grey,
                      ),
                      getIngredientsSelection(),
                    ].where((child) => child != null).toList(),
                  )),
              Text(
                'Escoge tu opcion favorita',
                textAlign: TextAlign.left,
              ),
              Container(
                height: screenSize.height / 3,
                child: new ListView.builder(
                  itemCount: combinations.length,
                  itemBuilder: (BuildContext context, int index) {
                    return new Card(
                      margin: EdgeInsets.only(left: 4, right: 4),
                      child: Container(
                          padding: EdgeInsets.all(2),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Text(combinations[index]['nombre']),
                                width: screenSize.width / 3,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      product.combinations[index].name +
                                          ' (' +
                                          f.format(double.parse(product
                                              .combinations[index].price
                                              .toString())) +
                                          ')',
                                      style: TextStyle(fontSize: 12),
                                    ),
                                    width: screenSize.width / 3,
                                  ),
                                  Container(
                                      child: IconButton(
                                          icon: Icon(Icons.arrow_drop_down),
                                          onPressed: () {
                                            showDemoActionSheet(
                                                context: context,
                                                title: product.name,
                                                message: combinations[index]
                                                ['nombre'],
                                                items: combinations[index],
                                                index: index);
                                          }))
                                ],
                              ),
                            ],
                          )),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      bottomNavigationBar: getBottomBar(screenSize),
    );
  }

  Widget getButtonOrder() {
    return new RaisedButton(onPressed: () {
      Navigator.pop(context, product);
    });
  }

  Widget getExtrasSelection() {
    if (extras.length > 0) {
      return Container(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 4, top: 2, right: 4, bottom: 2),
              child: Text('Añadir extras'),
            ),
            IconButton(
              icon: new Icon(Icons.arrow_forward_ios),
              onPressed: () => openExtrasScreen(),
            )
          ],
        ),
      );
    }
  }

  Widget getIngredientsSelection() {
    if (ingredients.length > 0) {
      return new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 4, right: 4, bottom: 2, top: 2),
                child: Text('Quitar ingredientes'),
              ),
              product.ingredients.length > 0
                  ? Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 4, bottom: 2, top: 2),
                    child: Text(
                      'No quiero: ',
                      style: TextStyle(
                          fontSize: 12, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 0, bottom: 2, top: 2),
                    child: Text(
                      Product.getRemoveIngredients(product, f),
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                ],
              )
                  : null,
            ].where((child) => child != null).toList(),
          ),
          IconButton(
            icon: new Icon(Icons.arrow_forward_ios),
            onPressed: () => openIngredientsScreen(),
          )
        ],
      );
    }
  }

  Widget getBottomBar(Size s) {
    if (double.parse(product.amount) > 0) {
      return new RoundedButton(
        buttonName: 'Añadir al pedido ' + f.format(amount),
        onTap: () {
          Product.addToCar(product, products);
          Navigator.pop(context, true);
        },
        width: s.width / 1.2,
        height: s.height / 13,
        bottomMargin: 10.0,
        borderWidth: 1.0,
        buttonColor: green,
      );
    }
  }

  void combinationChange(var combination, int index) {
    var combinationObj = Combination();
    combinationObj.id = combination['id'];
    combinationObj.valueId = combination['valores'][index]['id_valor'];
    combinationObj.name = combination['valores'][index]['valor'];
    combinationObj.price = combination['valores'][index]['precio'];
    for (int i = 0; i < product.combinations.length; i++) {
      if (product.combinations[i].id == combinationObj.id) {
        product.combinations[i] = combinationObj;
        updateAmount();
        return;
      }
    }
    product.combinations.add(combinationObj);
    updateAmount();
  }

  void addProductQuantity() {
    product.quantity += 1;
    updateAmount();
  }

  void removeProductQuantity() {
    if (product.quantity > 0) {
      product.quantity -= 1;
      updateAmount();
    }
  }

  void showDemoActionSheet(
      {BuildContext context,
        var items,
        String message,
        String title,
        int index}) {
    showCupertinoModalPopup<String>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: Text(title),
        message: Text(message),
        actions:
        getCupertinoActionSheetItems(combinations[index]['valores']),
        cancelButton: CupertinoActionSheetAction(
          child: const Text('Cancel'),
          isDefaultAction: true,
          onPressed: () {
            Navigator.pop(context, 'Cancel');
          },
        ),
      ),
    ).then((String value) {
      if (value != null) {
        setState(() {
          if (value != 'Cancel') {
            combinationChange(items, int.parse(value));
          }
        });
      }
    });
  }

  List<Widget> getCupertinoActionSheetItems(List items) {
    List<Widget> widgets = new List();
    for (int i = 0; i < items.length; i++) {
      widgets.add(CupertinoActionSheetAction(
        child: Text(
            items[i]['valor'] + ' (' + f.format((items[i]['precio'])) + ')'),
        onPressed: () {
          Navigator.pop(context, i.toString());
        },
      ));
    }
    return widgets;
  }

  void openExtrasScreen() async {
    if (await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new ExtrasScreen(
              extras: extras,
              product: product,
            )))) {
      Product.getExtras(product, f);
      updateAmount();
      setState(() {

      });
    }
  }

  void openIngredientsScreen() async {
    if (await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new IngredientsScreen(
              ingredients: ingredients,
              product: product,
            )))) {
      Product.getRemoveIngredients(product, f);
      updateAmount();
      setState(() {

      });
    }
  }

  void updateAmount() {
    amount = 0;
    for (var p in product.extras) {
      amount += ((double.parse(p['precio'].toString())) * product.quantity);
    }
    for (int i = 0; i < product.combinations.length; i++) {
      amount += (product.combinations[i].price * product.quantity);
    }
    amount += double.parse(product.amount) * product.quantity;

  }
}
