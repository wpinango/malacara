import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/restaurant.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../global.dart';
import '../../theme.dart';

class ExtrasScreen extends StatefulWidget {
  final List extras;
  final Product product;

  const ExtrasScreen({Key key, this.extras, this.product}) : super(key: key);

  @override
  ExtrasScreenState createState() =>
      new ExtrasScreenState(extras: extras, product: product);
}

class ExtrasScreenState extends State<ExtrasScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  List extras;
  Product product;
  var f;

  ExtrasScreenState({this.extras, this.product});

  @override
  initState() {
    super.initState();
    getSelectedCounty();
  }

  void getSelectedCounty() async {
    final prefs = await SharedPreferences.getInstance();
    Country selectedCountry =
    Country.fromJson(json.decode(prefs.get('selectedCountry')));
    setState(() {
      f = Global.getCurrencySymbol(selectedCountry);
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Extras',
          style: new TextStyle(
            color: Colors.white,
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context, true)),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: Column(
          children: <Widget>[
            new Container(
              height: screenSize.height / 2,
              child: new ListView.builder(
                itemCount: extras.length,
                itemBuilder: (BuildContext context, int index) {
                  return new ListTile(
                    title: Text(extras[index]['nombre'] +
                        ' (' +
                        f.format(double.parse(extras[index]['precio'])) +
                        ')'),
                    leading: CupertinoSwitch(
                        value: isExtrasExist(extras[index]['id']),
                        onChanged: (bool value) => {
                          setState(() {
                            if (value) {
                              product.extras.add(extras[index]);
                            } else {
                              product.extras.remove(extras[index]);
                            }
                          })
                        }),
                  );
                },
              ),
            )
          ],
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  bool isExtrasExist(String id) {
    bool value = false;
    for (var e in product.extras) {
      if (e['id'] == id) {
        value = true;
      }
    }
    return value;
  }
}
