
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/models/restaurant.dart';

import '../../theme.dart';

class IngredientsScreen extends StatefulWidget {
  final List ingredients;
  final Product product;

  const IngredientsScreen({Key key, this.ingredients, this.product}) : super(key: key);

  @override
  IngredientsScreenState createState() =>
      new IngredientsScreenState(ingredients: ingredients, product: product);
}

class IngredientsScreenState extends State<IngredientsScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  List ingredients;
  Product product;

  IngredientsScreenState({this.ingredients, this.product});

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Quitar ingredientes',
          style: new TextStyle(
            color: Colors.white,
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context, true)),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: Column(
          children: <Widget>[
            new Container(
              height: screenSize.height / 2,
              child: new ListView.builder(
                itemCount: ingredients.length,
                itemBuilder: (BuildContext context, int index) {
                  return new ListTile(
                    title: Text(ingredients[index]['nombre']),
                    leading: CupertinoSwitch(
                        value: isIngredientExist(ingredients[index]['id']),
                        onChanged: (bool value) => {
                          setState(() {
                            if (value) {
                              product.ingredients.add(ingredients[index]);
                            } else {
                              product.ingredients.remove(ingredients[index]);
                            }
                          })
                        }),
                  );
                },
              ),
            )
          ],
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  bool isIngredientExist(String id){
    bool value = false;
    for (var e in product.ingredients) {
      if (e['id'] == id) {
        value = true;
      }
    }
    return value;
  }
}
