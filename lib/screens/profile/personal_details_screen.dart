import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/models/credentials.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/service/validations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../api_url.dart';
import '../../global.dart';
import '../../theme.dart';
import '../../util.dart';

class PersonalDetailsScreen extends StatefulWidget {
  const PersonalDetailsScreen({Key key}) : super(key: key);

  @override
  PersonalDetailsScreenState createState() => new PersonalDetailsScreenState();
}

class PersonalDetailsScreenState extends State<PersonalDetailsScreen> {
  User user;
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  ScrollController scrollController = new ScrollController();
  var timeout = const Duration(seconds: 10);
  Validations validations = new Validations();
  String password;
  bool isInAsyncCall = false;
  bool autoValidate = false;
  bool newsletterBool;
  bool offersBool;

  @override
  initState() {
    super.initState();
    initVars();
  }

  void initVars() async {
    final prefs = await SharedPreferences.getInstance();
    user = User.fromJson(json.decode(prefs.get('user')));
    setState(() {
      newsletterBool = user.newsletter == '1' ? true : false;
      offersBool = user.offers == '1' ? true : false;
    });
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Detalles personales',
          style: new TextStyle(
            color: secondaryColor,
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: white,
            ),
            onPressed: () => {handleFormSubmitted()},
          ),
        ],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          controller: scrollController,
          child: new Container(
            height: screenSize.height,
            padding: new EdgeInsets.only(top: 8, right: 8, left: 8, bottom: 8),
            child: new Column(
              children: <Widget>[
                new Form(
                    key: formKey,
                    autovalidate: autoValidate,
                    child: new Column(
                      children: <Widget>[
                        new TextFormField(
                          decoration:
                              const InputDecoration(labelText: 'Nombre'),
                          keyboardType: TextInputType.text,
                          initialValue: user.name,
                          validator: validations.validateName,
                          onSaved: (String value) {
                            user.name = value;
                          },
                        ),
                        new TextFormField(
                          decoration:
                              const InputDecoration(labelText: 'Apellido'),
                          keyboardType: TextInputType.text,
                          initialValue: user.lastName,
                          validator: validations.validateLastName,
                          onSaved: (String value) {
                            user.lastName = value;
                          },
                        ),
                        new TextFormField(
                          decoration:
                              const InputDecoration(labelText: 'Correo'),
                          keyboardType: TextInputType.emailAddress,
                          initialValue: user.email,
                          validator: validations.validateEmail,
                          onSaved: (String value) {
                            user.email = value;
                          },
                        ),
                        new TextFormField(
                          decoration:
                              const InputDecoration(labelText: 'Password'),
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          validator: validations.validateTextInput,
                          onSaved: (String value) {
                            password = value;
                          },
                        ),
                        new TextFormField(
                          decoration:
                              const InputDecoration(labelText: 'Telefono'),
                          keyboardType: TextInputType.phone,
                          initialValue: user.mobileNumber,
                          validator: validations.validatePhone,
                          onSaved: (String value) {
                            user.mobileNumber = value;
                          },
                        ),
                        new TextFormField(
                          decoration:
                              const InputDecoration(labelText: 'Telefono fijo'),
                          keyboardType: TextInputType.phone,
                          initialValue: user.phone,
                          validator: validations.validatePhone,
                          onSaved: (String value) {
                            user.phone = value;
                          },
                        ),
                        Padding(padding: EdgeInsets.all(16)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Suscrito a las noticias de ' + Global.appName),
                            CupertinoSwitch(
                                value: newsletterBool,
                                onChanged: (bool value) => {
                                      setState(() {
                                        newsletterBool = value;
                                      })
                                    }),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Deseo recibir ofertas de ' + Global.appName),
                            CupertinoSwitch(
                                value: offersBool,
                                onChanged: (bool value) => {
                                      setState(() {
                                        offersBool = value;
                                      })
                                    }),
                          ],
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      print("buttonClicked");
      saveUserInfo();
    }
  }

  void saveUserInfo() async {
    showProgressDialog();
    user.newsletter = newsletterBool == true ? '1' : '0';
    user.offers = offersBool == true ? '1' : '0';
    var body = {
      'nombre': user.name,
      'apellidos': user.lastName,
      'username': user.email,
      'movil': user.mobileNumber,
      'telefono': user.phone,
      'newsletterBool': newsletterBool,
      'ofertasBool': offersBool,
      'password': Util.generateMd5(password),
      'newsletter': user.newsletter,
      'ofertas': user.offers,
      'token': user.token,
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    try {
      final response = await http
          .post(
              ApiURL.apiURL + ApiURL.urlUpdateUser.replaceAll('?', apiVersion),
              headers: headers,
              body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleInfoResponse(response.body);
      } else {
        handleInfoResponse("");
      }
    } catch (e) {
      handleInfoResponse("");
    }
  }

  void handleInfoResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        final prefs = await SharedPreferences.getInstance();
        Credentials credentials =
            new Credentials(user: user.email, password: password);
        setState(() {
          prefs.setString(Global.keyCredentials, json.encode(credentials));
          prefs.setString('user', json.encode(user));
          prefs.setBool('login', true);
        });
        if (await MyDialog.showNativePopUpWith2Buttons(context, 'Informacion',
            'Datos almacenados correctamente',
            'Cancelar',
            'Aceptar')) {
          Navigator.pop(context);
        }
      } else if (map['resultado']['code'] == -4) {
        showInSnackBar('Datos incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio temporalmente no disponible');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }
}
