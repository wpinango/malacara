import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../routes.dart';
import '../../theme.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key key}) : super(key: key);

  @override
  ProfileScreenState createState() => new ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  User user;
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;
  Country selectedCountry;

  @override
  initState() {
    super.initState();
    initVars();
  }

  void initVars() async {
    final prefs = await SharedPreferences.getInstance();
    selectedCountry =
        Country.fromJson(json.decode(prefs.get('selectedCountry')));
    user = User.fromJson(json.decode(prefs.get('user')));
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: onWillPop1,
      child: new Scaffold(
        key: scaffoldKey,
        appBar: new AppBar(
          title: new Text(
            'Mi cuenta',
            style: new TextStyle(
              color: secondaryColor,
              fontSize:
                  Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
            ),
          ),
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, true)),
          actions: <Widget>[],
          backgroundColor: primaryColor,
          iconTheme: IconThemeData(color: secondaryColor),
          elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
        ),
        body: ModalProgressHUD(
          child: SingleChildScrollView(
            controller: scrollController,
            child: new Container(
              height: screenSize.height,
              padding: new EdgeInsets.only(top: 8, right: 8, left: 8, bottom: 8),
              child: new Column(
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(8)),
                  new Divider(color: Colors.grey),
                  Container(
                    padding: EdgeInsets.all(4),
                    child: GestureDetector(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Icon(Icons.account_circle),
                              Padding(padding: EdgeInsets.all(8),),
                              Text('Detalles personales')
                            ],
                          ),
                          Icon(Icons.arrow_forward_ios)
                        ],
                      ),
                      onTap: () {
                        onPressed(context, '/PersonalDetails');
                      },
                    ),
                  ),
                  new Divider(color: Colors.grey),
                  Container(
                    padding: EdgeInsets.all(4),
                    child: GestureDetector(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Icon(Icons.location_on),
                              Padding(padding: EdgeInsets.all(8),),
                              Text('Direcciones de entrega')
                            ],
                          ),
                          Icon(Icons.arrow_forward_ios)
                        ],
                      ),
                      onTap: () {
                        onPressed(context, '/Address');
                      },
                    ),
                  ),
                  new Divider(color: Colors.grey),
                  Padding(padding: EdgeInsets.all(16)),
                  //new Divider(color: Colors.grey),
                  /*GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Icon(Icons.shopping_basket),
                            Text('Mis pedidos')
                          ],
                        ),
                        Icon(Icons.arrow_forward_ios)
                      ],
                    ),
                    onTap: () {
                      //Routes.showModalScreen(context, '/test');
                    },
                  ),
                  new Divider(color: Colors.grey),*/
                 /* GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Icon(Icons.favorite),
                            Text('Mis favoritos')
                          ],
                        ),
                        Icon(Icons.arrow_forward_ios)
                      ],
                    ),
                    onTap: () {

                    },
                  ),
                  new Divider(color: Colors.grey),
                  Padding(padding: EdgeInsets.all(16)),
                  new Divider(color: Colors.grey),
                  GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Icon(Icons.monetization_on),
                            Text('Descuentos')
                          ],
                        ),
                        Icon(Icons.arrow_forward_ios)
                      ],
                    ),
                    onTap: () {

                    },
                  ),
                  new Divider(color: Colors.grey),
                  GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Icon(Icons.beenhere),
                            Text('Puntos de fidelidad')
                          ],
                        ),
                        Icon(Icons.arrow_forward_ios)
                      ],
                    ),
                    onTap: () {

                    },
                  ),
                  new Divider(color: Colors.grey),*/
                ],
              ),
            ),
          ),
          inAsyncCall: isInAsyncCall,
          opacity: 0.5,
          progressIndicator: NativeLoadingIndicator(),
        ),
      ),
    );
  }

  onPressed(BuildContext context, String routeName) {
    Routes.showModalScreen(context, routeName);
  }

  Future<bool> onWillPop1() async {
    Navigator.pop(context, true);
  }
}
