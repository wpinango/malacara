import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:malacara/components/TextFields/input_field.dart';
import 'package:malacara/components/TextFields/password_input_field.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/components/buttons/text_button.dart';
import 'package:malacara/models/country.dart';
import 'package:malacara/models/credentials.dart';
import 'package:malacara/models/user.dart';
import 'package:malacara/screens/login/style.dart';
import 'package:malacara/service/validations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:ui';

import 'package:flutter/painting.dart';

import '../../api_url.dart';
import '../../global.dart';
import '../../routes.dart';
import '../../theme.dart';
import '../../util.dart';

class LoginScreen extends StatefulWidget {
  final bool isOnlyFinish;
  const LoginScreen({Key key, this.isOnlyFinish}) : super(key: key);

  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  Validations validations = new Validations();
  bool isInAsyncCall = false;
  var timeout = const Duration(seconds: 30);
  bool isHidePass = true;
  Credentials credentials = new Credentials();

  @override
  initState() {
    super.initState();
    requestVersion();
  }

  onPressed(String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void requestVersion() async {
    showProgressDialog();
    try {
      var body = {'version': 'v020300', 'platform': 'android', 'token': -1};
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post(ApiURL.apiURL + ApiURL.getVersion, headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleVersionResponse(response.body);
      } else {
        handleVersionResponse("");
      }
    } catch (e) {
      handleVersionResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleVersionResponse(final responseBody) async {
    String replace = "ï»¿";
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map =
          json.decode(responseBody.toString().replaceAll(replace, ''));
      String apiVersion = map['resultado']['vapi'];
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('apiVersion', apiVersion);
      for (var item in map['resultado']['businesses']) {
        Country country = new Country();
        country.name = item[0];
        country.url = item[1];
        country.code = item[2].toString().split('-')[1];
        country.completeCode = item[2];
        country.protocol = item[3];
      }
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  void doLogin() async {
    showProgressDialog();
    var body = {
      'username': credentials.user.trim(),
      'password': Util.generateMd5(credentials.password),
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    try {
      final response = await http
          .post(ApiURL.apiURL + ApiURL.doLogin.replaceAll('?', apiVersion),
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleLoginResponse(response.body);
      } else {
        handleLoginResponse("");
      }
    } catch (e) {
      handleLoginResponse("");
    }
  }

  void handleLoginResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        print(json.encode(map['resultado']));
        User user = User.fromJson(json.decode(json.encode(map['resultado'])));
        final prefs = await SharedPreferences.getInstance();
        print(json.encode(user));
        setState(() {
          prefs.setString(Global.keyCredentials, json.encode(credentials));
          prefs.setString('user', json.encode(user));
          prefs.setBool('login', true);
        });
        if (widget.isOnlyFinish) {
          Navigator.pop(context, 1);
        } else {
          Routes.replacementScreen(context, "/FindRest");
        }
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('El usuario o contraseña son incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio temporalmente no disponible');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      print("buttonClicked");
      doLogin();
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      body: ModalProgressHUD(
        child: SingleChildScrollView(
            controller: scrollController,
            child: new Container(
                width: screenSize.width,
                // padding: new EdgeInsets.all(32.0),
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/bg-carta-comidas.jpg"),
                      fit: BoxFit.cover),
                ),
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
                  child: Container(
                    width: screenSize.width,
                    color: Colors.black.withOpacity(0.5),
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          width: screenSize.width / 1.4,
                          height: screenSize.height,
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(padding: EdgeInsets.all(32),),
                              Container(
                                child: Image.asset('assets/logo.png'),
                                width: screenSize.width / 1.5,
                              ),
                              Padding(
                                padding: EdgeInsets.all(32),
                              ),
                              new Form(
                                key: formKey,
                                autovalidate: autoValidate,
                                child: new Column(
                                  children: <Widget>[
                                    new InputField(
                                        hintText: "Correo",
                                        obscureText: false,
                                        textInputType:
                                            TextInputType.emailAddress,
                                        textStyle: GoogleFonts.varelaRound(
                                            textStyle: textStyle),
                                        hintStyle: hintStyle,
                                        textCapitalization:
                                            TextCapitalization.none,
                                        textFieldColor: textFieldColor,
                                        icon: Icons.mail_outline,
                                        iconColor: Colors.grey,
                                        bottomMargin: 20.0,
                                        validateFunction:
                                            validations.validateEmail,
                                        onSaved: (String email) {
                                          credentials.user = email;
                                        }),
                                    new PasswordInputField(
                                        hintText: "Contraseña",
                                        obscureText: isHidePass,
                                        textInputType: TextInputType.text,
                                        textStyle: GoogleFonts.varelaRound(
                                            textStyle: textStyle),
                                        hintStyle: hintStyle,
                                        textFieldColor: textFieldColor,
                                        icon: Icons.lock_open,
                                        //iconColor: Colors.black,
                                        bottomMargin: 30.0,
                                        suffixIcon: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isHidePass = !isHidePass;
                                            });
                                          },
                                          child: Icon(!isHidePass
                                              ? Icons.visibility
                                              : Icons.visibility_off),
                                        ),
                                        validateFunction:
                                            validations.validatePassword,
                                        onSaved: (String password) {
                                          credentials.password = password;
                                        }),
                                    new RoundedButton(
                                      buttonName: "Entrar",
                                      onTap: handleFormSubmitted,
                                      width: screenSize.width / 1.3,
                                      height: 50.0,
                                      bottomMargin: 10.0,
                                      borderWidth: 0.0,
                                      buttonColor: secondaryColor,
                                    ),
                                  ],
                                ),
                              ),
                              new Column( 
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  new SimpleTextButton(
                                      buttonName: "Olvide mi Contraseña",
                                      onPressed: () => Routes.showModalScreen(
                                          context, '/Recover'),
                                      buttonTextStyle: GoogleFonts.varelaRound(
                                          textStyle: TextStyle(color: white))),
                                  new Divider(color: Colors.grey),
                                  new SimpleTextButton(
                                      buttonName: "CREAR CUENTA",
                                      onPressed: () => onPressed("/SignUp"),
                                      buttonTextStyle: GoogleFonts.varelaRound(
                                          textStyle: TextStyle(
                                              color: white,
                                              fontWeight: FontWeight.bold))),
                                ],
                              ),
                              new Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ))),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }
}
