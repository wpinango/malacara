
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../theme.dart';

class LogoutScreen extends StatefulWidget {
  const LogoutScreen({Key key}) : super(key: key);

  @override
  LogoutScreenState createState() => new LogoutScreenState();
}

class LogoutScreenState extends State<LogoutScreen> {
  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool isInAsyncCall = false;


  onPressed(String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          'Cerrar sesion',
          style: new TextStyle(
            color: secondaryColor,
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        iconTheme: IconThemeData(color: secondaryColor),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
            controller: scrollController,
            child: new Container(
              height: screenSize.height / 1.3,
              width: screenSize.width,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Container(
                      width: screenSize.width / 1.5,
                      height: screenSize.height / 10,
                      decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: ExactAssetImage('assets/logo.png')))),
                  Padding(padding: EdgeInsets.all(16)),
                  RoundedButton(
                    buttonName: 'Cerrar sesion',
                    onTap: () {
                      doLogout();
                    },
                    width: screenSize.width / 1.1,
                    height: screenSize.height / 13,
                    bottomMargin: 10.0,
                    borderWidth: 1.0,
                    buttonColor: secondaryColor,
                  ),
                ],
              ),
            )),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  void doLogout() async {
    if (await MyDialog.showNativePopUpWith2Buttons(
        context,
        'Advertencia',
        'Esta seguro de que desea cerrar la sesion?',
        'Cancelar',
        'Aceptar')) {
      final prefs = await SharedPreferences.getInstance();
      setState(() {

        prefs.setString('user', null);
        prefs.setString('selectedCountry', null);
        prefs.setBool('login', false);

      });
      Navigator.pop(context, true);
      //Routes.replacementScreen(context, '/PreMain');
    }
  }
}