import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:malacara/components/TextFields/input_field.dart';
import 'package:malacara/components/TextFields/password_input_field.dart';
import 'package:malacara/components/buttons/rounded_button.dart';
import 'package:malacara/dialogs/dialog.dart';
import 'package:malacara/service/validations.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../api_url.dart';
import '../../global.dart';
import '../../theme.dart';
import 'package:http/http.dart' as http;

class RecoverPassScreen extends StatefulWidget {
  const RecoverPassScreen({Key key}) : super(key: key);

  @override
  RecoverPassScreenState createState() => new RecoverPassScreenState();
}

class RecoverPassScreenState extends State<RecoverPassScreen> {

  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool autoValidate = false;
  Validations validations = new Validations();
  bool isInAsyncCall = false;
  String email;


  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery
        .of(context)
        .size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
            child: new Container(
              width: screenSize.width,
              padding: new EdgeInsets.all(32.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/fondo.png"), fit: BoxFit.cover),
              ),
              child: new Column(
                children: <Widget>[
                  new Container(
                    width: screenSize.width / 1.4,
                    height: screenSize.height,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(padding: EdgeInsets.all(32),),
                        Container(
                          child: Image.asset(
                              'assets/NEXI-VOY_logotipo_WEB_VERDE.png'),
                          width: screenSize.width / 1.5,
                        ),
                        Padding(
                          padding: EdgeInsets.all(24),
                        ),
                        new Form(
                          key: formKey,
                          autovalidate: autoValidate,
                          child: new Column(
                            children: <Widget>[
                              new InputField(
                                  hintText: "Correo",
                                  obscureText: false,
                                  textInputType: TextInputType.emailAddress,
                                  textStyle: GoogleFonts.varelaRound(
                                      textStyle: textStyle),
                                  hintStyle: hintStyle,
                                  textCapitalization: TextCapitalization.none,
                                  textFieldColor: textFieldColor,
                                  icon: Icons.mail_outline,
                                  iconColor: Colors.grey,
                                  bottomMargin: 20.0,
                                  validateFunction: validations.validateEmail,
                                  onSaved: (String email) {
                                    this.email = email;
                                  }),
                              new RoundedButton(
                                buttonName: "Recuperar",
                                onTap: handleFormSubmitted,
                                width: screenSize.width / 1.3,
                                height: 50.0,
                                bottomMargin: 10.0,
                                borderWidth: 0.0,
                                buttonColor: green,
                              ),
                            ],
                          ),
                        ),
                        Padding(padding: EdgeInsets.all(64),)
                      ],
                    ),
                  )
                ],
              ),
            )),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
    );
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      print("buttonClicked");
      requestPassRecover();
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void requestPassRecover() async {
    showProgressDialog();
    Map body = {
      'email': email,
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    try {
      final response = await http
          .post(ApiURL.apiURL + ApiURL.urlRecoverPass.replaceAll('?', apiVersion),
          headers: headers, body: map)
          .timeout(Global.timeout);
      print(response.body);
      if (response.body != "") {
        handleRecoverPassResponse(response.body);
      } else {
        handleRecoverPassResponse("");
      }
    } catch (e) {
      handleRecoverPassResponse("");
    }
  }

  void handleRecoverPassResponse(var responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      if (map['resultado']['code'] == 0) {
        if (await MyDialog.showNativePopUpWith2Buttons(context, 'Importante',
            'La contraseña fue envidad a su bandeja de correo', 'Cancelar',
            'Aceptar')) {
            Navigator.of(context).pop();
        }
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('El usuario o contraseña son incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio temporalmente no disponible');
      }
    } else if (responseBody == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

}