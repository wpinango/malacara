import 'package:malacara/models/restaurant.dart';

class ShoppingCart {
  List<Product> products = [];
  //double priceNet;
  //double priceGross;
  double totalAmount;

  void addProduct(Product p) {
    products.add(p);
  }

  void remProduct(Product p) {
    products.remove(p);
  }

  void calculate() {
    //priceNet = 0;
    //priceGross = 0;
    totalAmount = 0;
    products.forEach((p) {
      //priceNet += p.priceNet;
      //priceGross += p.priceGross;
      totalAmount += double.parse(p.amount);
    });
  }

  void clear() {
    products = [];
    //priceNet = 0;
    //priceGross = 0;
    //vatAmount = 0;
  }
}