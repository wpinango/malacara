class Business{
  String id;
  String logo;
  String name;
  String address;
  Map schedules;
  String startVacation;
  String endVacation;
  String open;
  String tmedio;
  String halfPrice;
  int favorite;
  List kitchenType;
  var transport;
  var minOrder;
  var free;

  Business({this.id, this.logo, this.open,this.name,this.address,this.endVacation,
    this.favorite,this.free,this.halfPrice,this.minOrder,this.schedules, this.kitchenType,
    this.startVacation,this.tmedio,this.transport});

  factory Business.fromJson(Map<String, dynamic> json) {
    return Business(
      id: json['id'],
      logo: json['logo'],
      name: json['nombre'],
      address: json['direccion'],
      schedules: json['lista_horarios'],
      startVacation: json['ini_vacaciones'],
      endVacation: json['end_vacaciones'],
      open: json['abierto'],
      tmedio: json['tmedio'],
      halfPrice: json['precio_medio'],
      favorite: json['favorito'],
      kitchenType: json['tipos_cocina'],
      transport: json['transport'],
      minOrder: json['minorder'],
      free: json['free']
    );
  }

}

class Schedules {
  String hora_inicio;
  String hora_fin;
}

class Kitchen {
  String id;
  String name;
}