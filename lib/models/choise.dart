import 'package:flutter/material.dart';

class Choice {
  const Choice({this.title, this.icon, this.id});

  final int id;
  final String title;
  final IconData icon;
}