class UserLocation{
  double latitude;
  double longitude;
  String location;

  UserLocation({this.latitude, this.longitude});

  toJson() {
    return {
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  factory UserLocation.fromJson(Map<String, dynamic> json) {
    return UserLocation(
        latitude: json['latitude'],
        longitude: json['longitude']
    );
  }
}