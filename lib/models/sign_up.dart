class SignUp {
  String name;
  String username;
  String lastName;
  String phone;
  String password;

  SignUp({this.phone, this.lastName, this.name, this.password, this.username});
  
  factory SignUp.fromJson(Map<String, dynamic> json) {
    return SignUp(
      name: json['nombre'],
      lastName: json['apellidos'],
      phone: json['movil'],
      password: json['password'],
      username: json['username']
    );
  }
  
  toJson() {
    return {
      'nombre' : name,
      'apellidos' : lastName,
      'movil' : phone,
      'password' : password,
      'username': username,
    };
  }

}