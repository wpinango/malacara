class Address {
  String lat;
  String lng;
  String cityId;
  String addressId;
  String address;
  String info;
  String direccion;

  Address(
      {this.addressId,
      this.cityId,
      this.address,
      this.lng,
      this.lat,
      this.info,
      this.direccion});

  toJson() {
    return {
      'id_direccion': addressId,
      'direccion': direccion,
      'address': address,
      'info':info,
      'id_city':cityId,
      'lat':lat,
      'lng':lng
    };
  }

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      lat: json['lat'],
      lng: json['lng'],
      address: json['address'],
      info: json['info'],
      cityId: json['id_city'],
      addressId: json['id_direccion'],
      direccion: json['direccion']
    );
  }


  /*userOrder.orderClientInfo = new OrderClientInfo();
  userOrder.orderClientInfo.cityId = address.cityId;
  userOrder.orderClientInfo.addressId = address.addressId;
  userOrder.orderClientInfo.paymentWayId = 3;
  userOrder.orderClientInfo.paypalId = '';
  userOrder.orderClientInfo.discountClientId = '';
  userOrder.orderClientInfo.comment = '';
  userOrder.orderClientInfo.discountGlobalId = '';*/

  /*for (var a in user.address) {
  Address address = new Address();
  address.address = a['address'];
  address.addressId = a['id_direccion'];
  address.cityId = a['id_city'];
  address.lat = a['lat'];
  address.lng = a['lng'];
  address.info = a['info'];
  values.add(address);
  }*/
}
