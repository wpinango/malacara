class User {
  int code;
  String token;
  String name;
  String lastName;
  String email;
  String phone;
  String mobileNumber;
  String newsletter;
  String offers;
  List address;
  List discounts;
  var orders;

  User(
      {this.code,
      this.token,
      this.name,
      this.lastName,
      this.discounts,
      this.address,
      this.email,
      this.mobileNumber,
      this.newsletter,
      this.offers,
      this.orders,
      this.phone});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      name: json['nombre'],
      code: json['code'],
      lastName: json['apellidos'],
      email: json['email'],
      phone: json['telefono'],
      mobileNumber: json['movil'],
      newsletter: json['newsletter'],
      offers: json['ofertas'],
      discounts: json['descuentos'],
      orders: json['pedidos'],
      address: json['direcciones'],
      token: json['token']
    );
  }

  Map<String, dynamic> toJson() => {
    'nombre': name,
    'apellidos': lastName,
    'email':email,
    'telefono': phone,
    'movil':mobileNumber,
    'newsletter':newsletter,
    'ofertas':offers,
    'descuentos':discounts,
    'pedidos':orders,
    'direcciones':address,
    'token':token,
    'code': code
  };
}
