class Restaurant {
  String id;
  String banner;
  String cityId;
  String logo;
  String name;
  String address;
  String open;
  Map schedules;
  String startVacation;
  String endVacation;
  String rest;
  String tmedio;
  String speciality;
  String foodType;
  String halfPrice;
  var minOrder;
  String payment;
  String productsImg;
  String mixing;
  List businessMixing;
  String reservations;
  String tableNumbers;
  String description;
  List photos;
  var video;
  var transport;
  var free;

  Restaurant(
      {this.name,
      this.minOrder,
      this.tmedio,
      this.address,
      this.logo,
      this.open,
      this.schedules,
      this.transport,
      this.startVacation,
      this.halfPrice,
      this.free,
      this.endVacation,
      this.banner,
      this.businessMixing,
      this.cityId,
      this.description,
      this.foodType,
      this.id,
      this.mixing,
      this.payment,
      this.photos,
      this.productsImg,
      this.reservations,
      this.speciality,
      this.tableNumbers,
      this.video,
      this.rest});

  factory Restaurant.fromJson(Map<String, dynamic> json) {
    return Restaurant(
      name: json['nombre'],
      id: json['id'],
      banner: json['banner'],
      cityId: json['id_ciudad'],
      logo: json['logo'],
      address: json['direccion'],
      open: json['abierto'],
      schedules: json['lista_horarios'],
      startVacation: json['ini_vacaciones'],
      endVacation: json['end_vacaciones'],
      rest: json['descanso'],
      speciality: json['especialidad'],
      foodType: json['tipo_comida'],
      tmedio: json['tmedio'],
      halfPrice: json['precio_medio'],
      minOrder: json['minorder'],
      payment: json['mcobro'],
      productsImg: json['img_productos'],
      mixing: json['mixing'],
      businessMixing: json['negocios_mixing'],
      reservations: json['reservas'],
      tableNumbers: json['numero_mesas'],
      description: json['descripcion'],
      photos: json['fotos'],
      video: json['video'],
      transport: json['transport'],
      free: json['free'],
    );
  }

  static bool isRestaurantClosed(Restaurant restaurant) {
    bool isValue;
    if (int.parse(restaurant.open) == 0) {
      //Cerrado
      isValue = true;
    } else if (int.parse(restaurant.open) == 1) {
      //Abierto
      isValue = false;
    }
    return isValue;
  }

  static int getClosedRestaurantsLength(List<Restaurant> restaurants) {
    int number = 0;
    for (var r in restaurants) {
      if (int.parse(r.open) == 0) {
        number++;
      }
    }
    return number;
  }

  static List<Restaurant> getClosedRestaurants(List<Restaurant> restaurants) {
    List<Restaurant> res = new List();
    for (var r in restaurants) {
      if (int.parse(r.open) == 0) {
        res.add(r);
      }
    }
    return res;
  }

  static double getTotalRestaurantAmount(
      Restaurant restaurant, List<Product> products) {
    return double.parse(Product.getTotalAmount(products).toStringAsFixed(2)) + double.parse(restaurant.transport);
  }
}

class MenuCard {
  String id;
  String name;
  List products;

  MenuCard({this.name, this.id, this.products});

  factory MenuCard.fromJson(Map<String, dynamic> json) {
    return MenuCard(
        id: json['id'], name: json['nombre'], products: json['productos']);
  }
}

class Product {
  String id;
  String name;
  int quantity;
  String type = '1';
  String amount;
  String observation;
  String description;
  List ingredients;
  List extras;
  List combinations;

  Product(
      {this.id,
      this.quantity,
      this.name,
      this.combinations,
      this.extras,
      this.ingredients,
      this.observation,
      this.amount,
      this.type,
      this.description});

  toJson() {
    return {
      'id_producto': id,
      'nombre': name,
      'cantidad': quantity,
      'tipo': type,
      'importe': amount,
      'observaciones': observation,
      'ingredientes': ingredients,
      'extras': extras,
      'combinaciones': combinations,
    };
  }

  static Product prepareData(var p) {
    Product product = new Product();
    if (p is Product) {
      product.id = p.id;
      product.name = p.name;
      product.quantity = p.quantity ?? 1;
      product.type = '1';
      product.amount = p.amount;
      product.description = p.description;
      product.observation = '';
      product.ingredients = p.ingredients;
      product.extras = p.extras;
      product.combinations = p.combinations;
    } else {
      product.id = p['id'];
      product.name = p['nombre'];
      product.quantity = p['cantidad'] ?? 1;
      product.type = '1';
      product.description = p['descripcion'];
      product.amount = p['precio'];
      product.observation = '';
      product.ingredients = [];
      product.extras = [];
      product.combinations = [];
    }
    return product;
  }

  static int getProductIndex(Product p, List<Product> products) {
    int index = -1;
    for (int i = 0; i < products.length; i++) {
      if (products[i].id == p.id) {
        index = i;
      }
    }
    return index;
  }

  static bool isProductExist(Product p, List<Product> products) {
    bool isExist = false;
    for (var a in products) {
      if (a.id == p.id) {
        isExist = true;
      }
    }
    return isExist;
  }

  static double getTotalAmount(List<Product> products) {
    double amount = 0;
    if (products.length > 0) {
      for (int i = 0; i < products.length; i++) {
        for (var p in products[i].extras) {
          amount +=
              ((double.parse(p['precio'].toString())) * products[i].quantity);
        }
        for (int j = 0; j < products[i].combinations.length; j++) {
          amount += (products[i].combinations[j].price * products[i].quantity);
        }
        amount += double.parse(products[i].amount) * products[i].quantity;
      }
    }
    return amount;
  }

  static void removeItemCar(var p, List<Product> products) {
    Product product = Product.prepareData(p);
    if (Product.isProductExist(product, products)) {
      if (products[Product.getProductIndex(product, products)].quantity == 1) {
        products.removeAt(Product.getProductIndex(product, products));
      } else {
        products[Product.getProductIndex(product, products)].quantity =
            products[Product.getProductIndex(product, products)].quantity - 1;
      }
    }
  }

  static void addToCar(Product p, List<Product> products) {
    products.add(p);
  }

  static void checkItemsInCar(var p, List<Product> products) {
    Product product = Product.prepareData(p);
    if (Product.isProductExist(product, products)) {
      products[Product.getProductIndex(product, products)].quantity =
          products[Product.getProductIndex(product, products)].quantity + 1;
    } else {
      addToCar(product, products);
    }
  }

  static double getProductAmount(Product product) {
    double amount = 0;
    for (var p in product.extras) {
      amount += ((double.parse(p['precio'].toString())) * product.quantity);
    }
    for (int j = 0; j < product.combinations.length; j++) {
      amount += (product.combinations[j].price * product.quantity);
    }
    amount += double.parse(product.amount) * product.quantity;
    return amount;
  }

  static int getProductType(var p, List<Product> products) {
    int count = 0;
    Product product = Product.prepareData(p);
    for (var i in products) {
      if (product.id == i.id) {
        count += i.quantity;
      }
    }
    return count;
  }

  static String getRemoveIngredients(Product product, var f) {
    String value = '';
    for (var p in product.ingredients) {
      value += p['nombre'] + ', ';
    }
    return value;
  }

  static String getExtras(Product product, var f) {
    String value = '';
    for (var p in product.extras) {
      value +=
          p['nombre'] + ' (' + f.format(double.parse(p['precio'])) + ')' + ', ';
    }
    return value;
  }

  static String getCombinations(Product product, var f) {
    String value = '';
    for (var p in product.combinations) {
      value =
          p.name + ' (' + f.format(double.parse(p.price.toString())) + ')\n';
    }
    return value;
  }

  static double getSubtotal(List<Product> products) {
    double amount = Product.getTotalAmount(products);
    return amount;
  }
}

class Combination {
  int id;
  String valueId;
  String name;
  var price;

  toJson() {
    return {
      'id': id,
      'valor': name,
      'precio': price,
      'id_valor': valueId,
    };
  }
}
