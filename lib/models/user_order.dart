import 'dart:convert';

class UserOrder {
  String discount;
  String scrapped;
  List order;
  String reference;
  String token;
  double totalAmount;
  OrderClientInfo orderClientInfo;

  UserOrder(
      {this.token,
      this.discount,
      this.order,
      this.reference,
      this.scrapped,
      this.totalAmount,
      this.orderClientInfo});

  toJson() {
    return {
      'descuento': discount,
      'desechado': scrapped,
      'pedido': (order),
      'referencia': reference,
      'token': token,
      'total_pedido': totalAmount,
      'info_pedido_cliente': orderClientInfo.toJson(),
    };
  }

  parseToOwn() {
    return {
      'descuento': discount,
      'desechado': scrapped,
      'pedido': jsonEncode(order),
      'referencia': reference,
      'token': token,
      'total_pedido': totalAmount,
      'info_pedido_cliente': orderClientInfo.toJson(),
    };
  }

  factory UserOrder.fromJson(Map<String, dynamic> json) {
    return UserOrder(
      token: json['token'],
      order: jsonDecode(json['pedido']),
      totalAmount: json['total_pedido'],
      discount: json['descuento'],
      scrapped: json['desechado'],
      reference: json['referencia'],
      orderClientInfo: OrderClientInfo.fromJson(json['info_pedido_cliente']),

    );
  }
}

class Order {
  List products;
  String businessId;
  String name;
  String transport;
  double transportFloat;
  double totalRestaurant;
  String minOrder;
  String free;
  String open;
  Map schedules;

  Order(
      {this.name,
      this.products,
      this.schedules,
      this.open,
      this.minOrder,
      this.transport,
      this.free,
      this.businessId,
      this.totalRestaurant,
      this.transportFloat});

  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
      products: json['productos'],
      businessId: json['id_negocio'],
      name: json['nombre'],
      transport: json['transporte'],
      transportFloat: json['transporteFloat'],
      totalRestaurant: json['total_restaurante'],
      minOrder: json['pedido_minimo'],
      free: json['free'],
      open: json['abierto'],
      schedules: json['lista_horarios'],
    );
  }

  toJson() {
    return {
      "productos":products,
      "id_negocio":businessId,
      "nombre":name,
      "transporte":transport,
      "transporteFloat":transportFloat,
      "total_restaurante":totalRestaurant,
      "pedido_minimo":minOrder,
      "free":free,
      "abierto":open,
      //"lista_horarios":schedules,
    };
  }
}

class OrderClientInfo {
  String comment;
  String date;
  String cityId;
  String discountClientId;
  String discountGlobalId;
  String addressId;
  int paymentWayId = 3;
  String paypalId;

  OrderClientInfo(
      {this.comment,
      this.cityId,
      this.date,
      this.addressId,
      this.discountGlobalId,
      this.discountClientId,
      this.paymentWayId,
      this.paypalId});

  toJson() {
    return {
      'comentarios': comment,
      'id_ciudad': cityId,
      'id_descuento_cliente': discountClientId,
      'id_descuentos_globales': discountGlobalId,
      'id_direccion': addressId,
      'id_forma_pago': paymentWayId,
      'id_paypal': paypalId,
      'fecha':date
    };
  }

  factory OrderClientInfo.fromJson(Map<String, dynamic> json) {
    return OrderClientInfo(
      comment: json['comentarios'],
      cityId: json['id_ciudad'],
      discountClientId: json['id_descuento_cliente'],
      discountGlobalId: json['id_descuentos_globales'],
      addressId: json['id_direccion'],
      paymentWayId: json['id_forma_pago'],
      paypalId: json['id_paypal'],
      date: json['fecha']
    );
  }
}
