class Country {
  String name;
  String code;
  String completeCode;
  String url;
  String protocol;

  Country({this.name, this.code, this.completeCode, this.protocol, this.url});

  Map<String, dynamic> toJson() => {
        'name': name,
        'code': code,
        'completeCode': completeCode,
        'url': url,
        'protocol': protocol
      };

  factory Country.fromJson(Map<String, dynamic> json) {
    return Country(
        name: json['name'],
        code: json['code'],
        url: json['url'],
        completeCode: json['completeCode'],
        protocol: json['protocol']);
  }
}
