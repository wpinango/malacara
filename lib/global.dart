import 'package:intl/intl.dart';
import 'package:malacara/api_url.dart';

import 'models/country.dart';

class Global{
  static String keyCountry = "country";
  static String keySelectedCountry = 'selectedCountry';
  static String keyCredentials = 'credentials';
  static String keyLogin = 'login';
  static String keyApiUrl = ApiURL.apiURL;//'api_url';
  static String appName = 'La Malacara';
  static String appVersion = 'v020300';
  static String platform = 'ios';
  static String orderDBPath = 'order.db';
  static var timeout = const Duration(seconds: 30);
  static bool isPaypalProduction = false;
  static bool isProduction = false;
  static bool isPosProduction = false;

  static const paypalConfig = {
    'business': 'sb-suahh1510181@personal.example.com',
  };

  static NumberFormat getCurrencySymbol(Country selectedCountry) {
    var f;
    if (selectedCountry.completeCode == 'es-ec') {
      f = new NumberFormat.simpleCurrency(locale: 'es_US',name: 'PEN');
    } else {
      f = new NumberFormat.simpleCurrency(locale: selectedCountry.completeCode);
    }
    return f;
  }
}