
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:malacara/screens/address/address_screen.dart';
import 'package:malacara/screens/business/business_screen.dart';
import 'package:malacara/screens/country/country_selected_screen.dart';
import 'package:malacara/screens/login/login_screen.dart';
import 'package:malacara/screens/login/logout_screen.dart';
import 'package:malacara/screens/login/recover_pass_screen.dart';
import 'package:malacara/screens/main/find_rest_screen.dart';
import 'package:malacara/screens/main/pre_main.dart';
import 'package:malacara/screens/order/order_history_screen.dart';
import 'package:malacara/screens/payment/order_success_screen.dart';
import 'package:malacara/screens/profile/personal_details_screen.dart';
import 'package:malacara/screens/profile/profile_screen.dart';
import 'package:malacara/screens/restaurant/restaurant_screen.dart';
import 'package:malacara/screens/signup/signup_screen.dart';
import 'package:malacara/screens/splash_screen.dart';
import 'package:malacara/theme.dart';

class Routes {

  var routes = <String, WidgetBuilder> {
    "/FindRest": (BuildContext context) => new FindRestScreen(),
    "/Login": (BuildContext context) => new LoginScreen(),
    "/SignUp": (BuildContext context) => new SignUpScreen(),
    "/Business": (BuildContext context) => new BusinessScreen(),
    "/Restaurant": (BuildContext context) => new RestaurantScreen(),
    "/CountrySelected":(BuildContext context) => new CountrySelectedScreen(),
    "/Profile":(BuildContext context) => new ProfileScreen(),
    "/PersonalDetails":(BuildContext context) => new PersonalDetailsScreen(),
    "/Address":(BuildContext context) => new AddressScreen(),
    "/PaymentSuccess":(BuildContext context) => new PaymentSuccessScreen(),
    "/Logout": (BuildContext context) => new LogoutScreen(),
    "/OrderHistory" : (BuildContext context) => new OrderHistoryScreen(),
    "/PreMain" : (BuildContext context) => new PreMainScreen(),
    "/Splash" : (BuildContext context) => new FirstScreen(),
    "/Recover":(BuildContext context) => new RecoverPassScreen(),
    //"/test":(BuildContext context) => new MapScreen(),
  };

  Routes() {
    runApp(new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "NexoVoy",
      home: new FirstScreen(),
      routes: routes,
      theme: ThemeData(
        primaryColor: primaryColor,
        appBarTheme: AppBarTheme(color: primaryColor),
        iconTheme: IconThemeData(color: secondaryColor),

      ),
    ));
  }

  static replacementScreen(BuildContext context, String routeName) {
    Navigator.pushReplacementNamed(context, routeName);
  }

  static showModalScreen(BuildContext context, String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }

}
