import 'global.dart';

class ApiURL {
  static final String apiURL =
      'https://delivery.lamalacara.com/panel/ws/aloha24'; //'https://lamalacara.com/delivery/panel/ws/aloha24';//'http://aloha24.com/alohages/ws/aloha24';//'http://app.nexivoy.com/administracion/ws/aloha24';
  static final String getVersion = '/ws_getVersion.php';
  static final String doLogin = '/?/ws_getAuth.php';
  static final String urlGetBusinessList = '/?/ws_getListaNegocios.php';
  static final String urlGetBusinessData = '/?/ws_getDatosNegocio.php';
  static final String urlSetOrder = '/?/ws_setPedido.php';
  static final String urlUpdateUser = '/?/ws_updUser.php';
  static final String urlRegister = '/?/ws_setUser.php';
  static final String urlLogout = '/?/ws_setLogout.php';
  static final String urlSetDirection = '/?/ws_setDireccion.php';
  static final String urlCheckAddress = '/?/ws_chkAddress.php';
  static final String urlRecoverPass = '/?/ws_setPass.php';
  static final String trackOrder = '/?/ws_getInfoFreelance.php';
  static final String urlEncodedData = '/?/wsCreditCard.php';

  static final String apiUrlPosPayment = Global.isPosProduction
      ? "https://sis.redsys.es/sis/rest/trataPeticionREST"
      : 'https://sis-t.redsys.es:25443/sis/rest/trataPeticionREST';
}
